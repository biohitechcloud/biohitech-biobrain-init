#!/bin/bash

# region Global Variables
TEMPORARY_WORKING_DIRECTORY=/var/tmp/biobrain

API_KEY=dd7a0d38-1a8d-45b2-8f73-9d8aaa683447
API_KEY_HEADER="x-biohitech-auth-token: $API_KEY"

BUILD_TYPE=""
DISK_INTERNAL=""
DISK_INTERNAL_PARTITION=""
DISK_INTERNAL_MOUNT="/media/internal"
DISK_INTERNAL_TEMP="$DISK_INTERNAL_MOUNT/var/tmp/biobrain"
DISK_USB=""
DISK_USB_MOUNT="/run/initramfs/memory/data"


DEPLOYMENT_BASE_URL=https://deploy.biohitechcloud.com
DEPLOYMENT_API_BASE_URL=https://deploy.biohitechcloud.com/api

BASE_OS_IMAGE_DIR="${DISK_USB_MOUNT}/tmp/images"
BASE_OS_IMAGE_FILE=""
BASE_OS_IMAGE_HASH=""
BASE_OS_IMAGE_URL="$DEPLOYMENT_API_BASE_URL/1.0/biobrain-images/download"


INIT_SCRIPT="$TEMPORARY_WORKING_DIRECTORY/master-image-init.sh"
INIT_SCRIPT_URL=https://biobrain.biohitechcloud.com/init/master-image-init

MASTER_IMAGE_BASE=""
MASTER_IMAGE_CATEGORY=""
MASTER_IMAGE_GENERATION=""
MASTER_IMAGE_VERSION=""
MASTER_IMAGE_DATE=""

VALID_BASE_IMAGES="ubuntu1204,ubuntu1404,ubuntu1604,ubuntu1804"
VALID_CATEGORIES="core,core-nuc,kiosk"
VALID_GENERATIONS="g1,g2,g3"

UPLOAD_FILE="$DISK_USB_MOUNT/biobrain.img.gz"
UPLOAD_TYPE_ID=""
UPLOAD_VERSION=""
UPLOAD_DATE=""


# endregion

# region Script Functions

function main() {
    if [[ "$(tty)" != "/dev/tty1" ]]; then
        exit
    fi

    scriptInit
    promptForBuildType

    if [[ "$BUILD_TYPE" == "build-new-master-image" ]]; then
        buildNewMasterImage
    elif [[ "$BUILD_TYPE" == "upload-master-image" ]]; then
        uploadMasterImage
    elif [[ "$BUILD_TYPE" == "upload-base-os-image" ]]; then
        uploadBaseOsImage
    else
        fail "Unknown Build Type: $BUILD_TYPE"
    fi

}

function buildNewMasterImage() {
    rootCheck
    createTemporaryWorkingDirectory
    unmountPartitions
    detectInternalDrive || fail "Unable to Detect Internal Drive"
    promptNewMasterImage
    downloadInitScript
    downloadBaseOsImage
    writeBaseOsImageToInternalDrive
    detectInternalDrivePartition || fail "Unable to Detect Internal Drive Partition"
    mountInternalDrivePartition
    writeInitPropertiesToInternalDrive
    writeInitScriptToInternalDrive
    runInitScriptAutomatically
    writeNetworkConfigToInternalDrive
    removeUdevNetRules
    sync
    awaitUsbDriveRemoval
    reboot
}


function awaitUsbDriveRemoval() {
    echo "BASE OS Installed.  Please remove the USB Drive to reboot and then login as bht"

    sleepTime=0
    diskUsbRemoved=""

    while [[ "$diskUsbRemoved" == "" ]]; do
        sleep ${sleepTime}
        detectUsbDrive || diskUsbRemoved="1" > /dev/null 2>&1
        sleepTime=1
    done
}

function createTemporaryWorkingDirectory() {
    echo "Creating Temporary Working Directory"
    mkdir -p ${TEMPORARY_WORKING_DIRECTORY} || failc
}

function detectInternalDrive() {
    echo "Detecting Internal Drive"

    DISK_INTERNAL=$(lsblk -b -o SIZE,KNAME,TYPE,SUBSYSTEMS,PKNAME | grep disk | grep -E ':mmc_host:|:scsi:pci([[:space:]]|$)' | sort -n | tail -1 | awk '{print $2}' || failc)

    if [[ "$DISK_INTERNAL" == "" ]]; then
        return 1
    fi

    return 0
}

function detectInternalDrivePartition() {
    # check for logic volumes first
    DISK_INTERNAL_PARTITION=$(lvs --units b -o lv_size,lv_path,devices | grep "\/${DISK_INTERNAL}" | sort -n | tail -1 | awk '{print $2}' | cut -d/ -f3-)

    if [[ "$DISK_INTERNAL_PARTITION" == "" ]]; then
        DISK_INTERNAL_PARTITION=$(lsblk -b -o SIZE,KNAME,TYPE,SUBSYSTEMS,PKNAME | grep part | grep "\s${DISK_INTERNAL}$" | sort -n | tail -1 | awk '{print $2}' || failc)
    fi

    if [[ "$DISK_INTERNAL_PARTITION" == "" ]]; then
        return 1
    fi

    return 0
}

function detectUsbDrive() {
    DISK_USB=$(lsblk -b -o SIZE,KNAME,TYPE,SUBSYSTEMS,PKNAME | grep disk | grep ':usb:' | sort -n | tail -1 | awk '{print $2}' || failc)

    if [[ "$DISK_USB" == "" ]]; then
        return 1
    fi

    return 0
}

function downloadInitScript() {
    echo "Downloading BioBrain Init Script"

    curlc -o ${INIT_SCRIPT} -w '%{response_code}' ${INIT_SCRIPT_URL} | httpStatusCheck || failc

    chmod +x ${INIT_SCRIPT} || failc
    sync
}

function downloadBaseOsImage() {
    echo "Downloading Base OS Image"

    BASE_OS_TYPE="os-$MASTER_IMAGE_CATEGORY-$MASTER_IMAGE_BASE"
    deployApiCurl -o ${TEMPORARY_WORKING_DIRECTORY}/latest.json -w '%{response_code}' "/1.0/biobrain-images/$BASE_OS_TYPE/latestPath" | httpStatusCheck || failc
    LATEST_PATH="$(cat ${TEMPORARY_WORKING_DIRECTORY}/latest.json | jq -r .path || failc)"

    deployApiCurl -o ${TEMPORARY_WORKING_DIRECTORY}/sha256.json -w '%{response_code}' "/1.0/biobrain-images/sha256?path=$LATEST_PATH" | httpStatusCheck || failc
    BASE_OS_IMAGE_HASH="$(cat ${TEMPORARY_WORKING_DIRECTORY}/sha256.json | jq -r .sha256 || failc)"

    mkdir -p ${BASE_OS_IMAGE_DIR} || failc

    hash=""

    if [[ -f ${BASE_OS_IMAGE_FILE} ]]; then
        hash=$(sha256sum ${BASE_OS_IMAGE_FILE} | awk '{print $1}' || failc)
    fi

    echo "Local Hash: $hash"
    echo "Remote Hash: $BASE_OS_IMAGE_HASH"

    if [[ "$hash" == "$BASE_OS_IMAGE_HASH" ]]; then
        echo "Hashes match. Skipping download."
    else
        echo "Requesting new image."
        curlc -H "Accept: application/octet-stream" -H "$API_KEY_HEADER" -o ${BASE_OS_IMAGE_FILE} -w '%{response_code}' "${BASE_OS_IMAGE_URL}?path=$LATEST_PATH" | httpStatusCheck || failc
    fi
    sync
}

function promptNewMasterImage() {
    echo ""

    while [[ -z "$MASTER_IMAGE_BASE" ]]; do
        read -p "Enter Base OS Image (eg. ubuntu1804): " MASTER_IMAGE_BASE
        validateContains MASTER_IMAGE_BASE "$MASTER_IMAGE_BASE" "$VALID_BASE_IMAGES" || MASTER_IMAGE_BASE=""
    done

    while [[ -z "$MASTER_IMAGE_CATEGORY" ]]; do
        read -p "Enter Category (eg. core,kiosk): " MASTER_IMAGE_CATEGORY
        validateContains MASTER_IMAGE_CATEGORY "$MASTER_IMAGE_CATEGORY" "$VALID_CATEGORIES" || MASTER_IMAGE_CATEGORY=""
    done

    while [[ -z "$MASTER_IMAGE_GENERATION" ]]; do
        read -p "Enter Generation (eg. g1,g2,g3): " MASTER_IMAGE_GENERATION
        validateContains MASTER_IMAGE_GENERATION "$MASTER_IMAGE_GENERATION" "$VALID_GENERATIONS" || MASTER_IMAGE_GENERATION=""
    done

    while [[ -z "$MASTER_IMAGE_VERSION" ]]; do
        read -p "Enter Version (eg. 1.0.0): " MASTER_IMAGE_VERSION
    done

    BASE_OS_IMAGE_FILE="$BASE_OS_IMAGE_DIR/os.$MASTER_IMAGE_CATEGORY.$MASTER_IMAGE_BASE.img.gz"
}

function mountInternalDrivePartition() {
    echo "Mounting Internal Drive"

    mkdir -p "$DISK_INTERNAL_MOUNT" || failc
    mount "/dev/$DISK_INTERNAL_PARTITION" "$DISK_INTERNAL_MOUNT" || failc

    mkdir -p ${DISK_INTERNAL_TEMP} || failc
}

function removeUdevNetRules() {
    file="$DISK_INTERNAL_MOUNT/etc/udev/rules.d/70-persistent-net.rules"

    if [[ -f "$file" ]]; then
		echo "Deleting 70-persistent-net.rules..."
        rm -f "$file" || failc
	fi
}

function runInitScriptAutomatically() {
    echo "/var/tmp/biobrain/master-image-prompt.sh" >> ${DISK_INTERNAL_MOUNT}/home/bht/.bashrc || failc
}


function uploadBaseOsImage() {
    echo "Uploading Base OS Image"
    uploadImage promptForBaseImageProperties
}

function uploadMasterImage() {
    echo "Uploading Master Image"
    uploadImage setMasterImageUploadProperties
}

function uploadImage() {
    setter="$1"
    rootCheck
    createTemporaryWorkingDirectory
    unmountPartitions
    detectInternalDrive || fail "Unable to Detect Internal Drive"
    detectInternalDrivePartition || fail "Unable to Detect Internal Drive Partition"
    mountInternalDrivePartition
    ${setter}
    createUploadImage
    scpUploadImage
    releaseUploadImage
    removeUploadImage
    sync
    echo "Done."
}

function createUploadImage() {
    echo "Creating Upload Image"
    cat /dev/${DISK_INTERNAL} | gzip -c > "$UPLOAD_FILE" || failc
}

function removeUploadImage() {
    echo "Removing Upload Image"
    rm  "$UPLOAD_FILE" || failc
}

function promptForBaseImageProperties() {
    imageBase=""
    imageCategory=""
    imageVersion=""

    while [[ -z "$imageBase" ]]; do
        read -p "Enter Base OS Image (eg. ubuntu1804): " imageBase
        validateContains imageBase "$imageBase" "$VALID_BASE_IMAGES" || imageBase=""
    done

    while [[ -z "$imageCategory" ]]; do
        read -p "Enter Category (eg. core,kiosk): " imageCategory
        validateContains imageCategory "$imageCategory" "$VALID_CATEGORIES" || imageCategory=""
    done

    UPLOAD_TYPE_ID="os-$imageCategory-$imageBase"
    UPLOAD_VERSION=""
    UPLOAD_DATE="$(date '+%Y-%m-%dT%H:%M:00')"
}

function setMasterImageUploadProperties {
    echo "Setting Upload Properties"
    imageVersion="$(cat ${DISK_INTERNAL_MOUNT}/opt/bht/etc/static.properties | grep 'image\.version' | cut -d= -f2 || failc)"
    imageCategory="$(echo ${imageVersion} | cut -d. -f2 || failc)"
    imageGeneration="$(echo ${imageVersion} | cut -d. -f3 || failc)"
    imageDate="$(echo ${imageVersion} | rev | cut -d. -f1 | rev || failc)"

    UPLOAD_TYPE_ID="master-$imageCategory-$imageGeneration"
    UPLOAD_VERSION="$(echo ${imageVersion}|  cut -d. -f4- | rev | cut -d. -f2- | rev || failc)"
    UPLOAD_DATE="$(dateParseAndFormat "$imageDate" %Y%m%d%H%M  %Y-%m-%dT%H:%M:00 || failc)"
}


function releaseUploadImage() {
    echo "Releasing Upload Image, typeId=$UPLOAD_TYPE_ID,version=$UPLOAD_VERSION,dateTime=$UPLOAD_DATE"

    json="{}"
    json=$(echo "$json" | jq ".typeId = \"$UPLOAD_TYPE_ID\"" || failc)
    json=$(echo "$json" | jq ".version = \"$UPLOAD_VERSION\"" || failc)
    json=$(echo "$json" | jq ".dateTime = \"$UPLOAD_DATE\"" || failc)

    deployApiCurl -o ${TEMPORARY_WORKING_DIRECTORY}/release -w '%{response_code}' -X POST --data-binary "$json" /1.0/biobrain-images/upload | httpStatusCheck || failc
}


function scpUploadImage() {
    echo "Sending Image to server"

    sshConfig=${TEMPORARY_WORKING_DIRECTORY}/ssh_config

cat << EOF > ${sshConfig} || failc
Host www.biohitechcloud.com
    StrictHostKeyChecking no
    UserKnownHostsFile /dev/null

Host digester-admin-prod.bht
    StrictHostKeyChecking no
    UserKnownHostsFile /dev/null
    ProxyCommand sshpass -p 'edwinhoch!!!' ssh -F ${sshConfig} bht@www.biohitechcloud.com nc %h %p 2>/dev/null
EOF

    # TODO: don't use password
    sshpass -p 'edwinhoch!!!' scp -F ${sshConfig} "$UPLOAD_FILE" bht@digester-admin-prod.bht:/tmp || failc

    rm ${sshConfig}
}

function promptForBuildType() {
cat << EOF
-------------------------------------------------------------------------------
Available Options:

1) Build New Master Image
2) Upload Master Image
3) Upload Base OS Image
4) Exit to Shell
-------------------------------------------------------------------------------

EOF

    BUILD_TYPE=""
    buildTypeNumber=""

    while [[ -z "$BUILD_TYPE" ]]; do
        read -p "Please type the number and then press Enter: " buildTypeNumber

        if [[ "$buildTypeNumber" == "1" ]]; then
            BUILD_TYPE="build-new-master-image"
        elif [[ "$buildTypeNumber" == "2" ]]; then
            BUILD_TYPE="upload-master-image"
        elif [[ "$buildTypeNumber" == "3" ]]; then
            BUILD_TYPE="upload-base-os-image"
        elif [[ "$buildTypeNumber" == "4" ]]; then
            exit
        else
            echo "Invalid menu option"
        fi
    done
}


function writeBaseOsImageToInternalDrive() {
    echo "Writing Base OS Image to Internal Drive"

    gunzip -c ${BASE_OS_IMAGE_FILE} > /dev/${DISK_INTERNAL} || failc
    partprobe /dev/${DISK_INTERNAL} || failc
    sync
}

function writeInitPropertiesToInternalDrive() {
    file="${DISK_INTERNAL_TEMP}/init.properties"
    MASTER_IMAGE_DATE="$(date '+%Y%m%d%H%M')"
    echo "MASTER_IMAGE_VERSION=master.$MASTER_IMAGE_CATEGORY.$MASTER_IMAGE_GENERATION.$MASTER_IMAGE_VERSION.$MASTER_IMAGE_DATE" > "$file"
}

function writeInitScriptToInternalDrive() {
    file="$DISK_INTERNAL_TEMP/master-image-init.sh"
    cp "$INIT_SCRIPT" "$file" || failc
    chmod +x "$file" || failc

    file="$DISK_INTERNAL_TEMP/master-image-prompt.sh"

cat << EOF > ${file} || failc
#!/bin/bash

if [[ "\$(tty)" != "/dev/tty1" ]]; then
    exit
fi

echo "--------------------------------------------------------------------------------"
echo " To initialize the master image, please run:"
echo " sudo /var/tmp/biobrain/init.sh"
echo "--------------------------------------------------------------------------------"
EOF

    chmod +x "$file" || failc


 file="$DISK_INTERNAL_TEMP/init.sh"

cat << EOF > ${file} || failc
#!/bin/bash

/var/tmp/biobrain/master-image-init.sh 2>&1 | tee /var/tmp/biobrain/master-image-init.log
EOF

    chmod +x "$file" || failc
}

function writeNetworkConfigToInternalDrive() {
    echo "Writing Network Config to Internal Drive"
    cloudInitFile=${DISK_INTERNAL_MOUNT}/etc/netplan/50-cloud-init.yaml
    cloudInitTmpFile=/tmp/50-cloud-init.yaml

    if [[ -f ${cloudInitFile} ]]; then
        interfaces="$(ifconfig -a | grep -E -o '^en(p|o)[[:digit:]]+(s[[:digit:]]+)?')"
        echo 'network:' > ${cloudInitTmpFile} || failc
        echo '    version: 2' >> ${cloudInitTmpFile} || failc
        echo '    ethernets:' >> ${cloudInitTmpFile} || failc

        for interface in ${interfaces}; do
            echo "        $interface:" >> ${cloudInitTmpFile} || failc
            echo "            dhcp4: true" >> ${cloudInitTmpFile} || failc
            echo "            dhcp6: true" >> ${cloudInitTmpFile} || failc
            echo "            nameservers:" >> ${cloudInitTmpFile} || failc
            echo "                addresses: [8.8.8.8, 8.8.4.4]" >> ${cloudInitTmpFile} || failc
        done

        cp ${cloudInitTmpFile} ${cloudInitFile} || failc
        echo 'network: {config: disabled}' > ${DISK_INTERNAL_MOUNT}/etc/cloud/cloud.cfg.d/99-disable-network-config.cfg || failc
    fi
}
# endregion


# region Utility Functions
function curlc() {
    curl -s -L -H "User-Agent: ${BASH_SOURCE[1]}" "$@"
}

function curlJson() {
    curlc -H 'Content-Type: application/json;charset=UTF-8' -H 'Accept: application/json' "$@"
}

function dateParseAndFormat() {
    date="$1"
    inputFormat="$2"
    outputFormat="$3"

    echo -n "$date" | python -c "import datetime,sys;print(datetime.datetime.strptime(sys.stdin.read(), '$inputFormat').strftime('$outputFormat'))"
}

function deployApiCurl() {
    # last argument
    url=$(urlQualify "${@: -1}" ${DEPLOYMENT_API_BASE_URL})

    # remove last argument
    set -- "${@: 1: $#-1}"

    curlJson -H "$API_KEY_HEADER" "$@" ${url}
}

function fail() {
    echo "ERROR: $1"
    exit 1;
}

function failc() {
    line=${BASH_LINENO[0]}
    func=$(sed "${line}q;d" ${BASH_SOURCE[0]})
    fail "Command Failed - $func" ${FUNCNAME[1]} ${BASH_LINENO[0]}
}

function httpStatusCheck() {
    validStatusCodes=$1
    statusCode=$2

    if [[ ! -t 0 ]]; then
        statusCode=$(cat -)
    fi

    if [[ "$statusCode" == "" ]]; then
        statusCode="$validStatusCodes"
        validStatusCodes=""
    fi

    if [[ "$validStatusCodes" == "" ]]; then
        validStatusCodes="200"
    fi

    if [[ "$validStatusCodes" != *"$statusCode"* ]]; then
        return 1
    fi

    echo -n ""

    return 0
}

function rootCheck() {
    echo "Performing Root Check"

    if [[ "$EUID" -ne 0 ]]; then
        fail "This script must be run as root"
    fi
}

function scriptInit() {
    trap 'exit 1' TERM
}


function validateNotEmpty() {
    name=$1
    value=$2

    if [[ "$value" == "" ]]; then
        echo "$name must not be empty"
        return 1
    fi
}

function validateContains() {
    name=$1
    value=$2
    haystack=$3

    if [[ "$value" == "" ]] || [[ "$haystack" != *"$value"* ]]; then
        echo "Invalid $name [$value]: must be one of [$haystack]"
        return 1
    fi
}

function unmountPartitions() {
    echo "Unmounting existing partitions"
    mounts=$(lsblk | grep -v initramfs | grep -E '\s+part\s+[^\s]' | awk '{print $NF}')

    for mount in ${mounts}; do
        echo "Unmounting $mount"
        umount "$mount" || fail "Unable to unmount $mount"
    done
}

function urlQualify() {
    url=$1
    base=$2

    if [[ "${url:0:1}" == "/" ]]; then
        url="$base$url"
    fi

    echo -n ${url}
}
# endregion


main "$@"
