#!/bin/bash


###############################################################################
# Installs Master Image and BioBrain Software Via USB Stick
###############################################################################

# region Global Variables
TEMPORARY_WORKING_DIRECTORY=/var/tmp/biobrain

API_KEY=dd7a0d38-1a8d-45b2-8f73-9d8aaa683447
API_KEY_HEADER="x-biohitech-auth-token: $API_KEY"

DEPLOYMENT_API_BASE_URL=https://deploy.biohitechcloud.com/api
DIGESTER_ADMIN_BASE_URL=https://digester-admin.biohitechcloud.com

DISK_INTERNAL=""
DISK_INTERNAL_PARTITION=""
DISK_INTERNAL_MOUNT="/media/internal"
DISK_INTERNAL_TEMP="$DISK_INTERNAL_MOUNT/var/tmp/biobrain"

DISK_USB=""
DISK_USB_PARTITION=""
DISK_USB_MOUNT="/media/usb"

MACHINE_KEY=""

MASTER_IMAGE_DIR=${DISK_USB_MOUNT}/tmp/images
MASTER_IMAGE_FILE=""
MASTER_IMAGE_HASH=""
MASTER_IMAGE_TYPE=""
MASTER_IMAGE_URL=""

INSTALL_ID=""

INIT_PROPERTIES_FILE=~/init.properties
INIT_PROPERTIES=""


INIT_SCRIPT="$TEMPORARY_WORKING_DIRECTORY/biobrain-init.sh"
INIT_SCRIPT_URL=https://biobrain.biohitechcloud.com/init/biobrain-init

IP_ADDRESS=""

VERIFY_SCRIPT="$TEMPORARY_WORKING_DIRECTORY/biobrain-init-verify.sh"
VERIFY_SCRIPT_URL=https://biobrain.biohitechcloud.com/init/biobrain-init-verify


USB_PROPERTIES_FILE="$DISK_USB_MOUNT/etc/usb.properties"
USB_KEY=""


SERIAL_NUMBER=""


SCRIPT_FAIL_FUNCTION="onFail"
SCRIPT_FAIL_MESSAGE='BioBrain Installation Failed'
SCRIPT_PASS_MESSAGE='BioBrain Installation Succeeded'

BIOBRAIN_SOFTWARE_VERSION=""

# endregion

# region Script Functions

function main() {
    if [[ "$(tty)" != "/dev/tty1" ]]; then
        exit
    fi

    scriptInit
    log "Installing BioBrain"
    rootCheck
    unmountPartitions
    createTemporaryWorkingDirectory
    detectInternalDrive || fail "Unable to Detect Internal Drive"
    detectUsbDrive || fail "Unable to Detect USB Drive"
    detectUsbPartition || fail "Unable to Detect USB Partition"
    detectSerialNumber || fail "Unable to Detect Serial Number"
    detectExistingMachineKey
    mountUsbDrivePartition
    loadUsbProperties
    createInstall
    awaitInitProperties
    startInstall
    loadInitProperties
    setUsbProperties
    removeUdevNetRules
    downloadInitScript
    downloadVerifyScript
    downloadBioBrainSoftware
    downloadMasterImage
    writeMasterImageToInternalDrive
    detectInternalDrivePartition || fail "Unable to Detect Internal Drive Partition"
    mountInternalDrivePartition
    copyInitPropertiesToInternalDrive
    copyInitScriptToInternalDrive
    copyBioBrainSoftwareToInternalDrive
    copyVerifyScriptToInternalDrive
    writeNetworkConfigToInternalDrive
    createLocalInitScript
    awaitUsbDriveRemoval
    pass
    cp "$DISK_USB_MOUNT/tmp/logs/biobrain-install.log" ${DISK_INTERNAL_MOUNT}/opt/bht/logs/biobrain-install.log.1
    sync
    doReboot
}

function awaitInitProperties() {
    step "Awaiting Configuration"

    sleepTime=0

    while [[ "$INIT_PROPERTIES" == "" ]]; do
        if [[ -f ${TEMPORARY_WORKING_DIRECTORY}/init.properties ]]; then
            rm ${TEMPORARY_WORKING_DIRECTORY}/init.properties
        fi

        sleep ${sleepTime}
        log "Attempting to Retreive Configuration"
        deployApiCurl -o ${TEMPORARY_WORKING_DIRECTORY}/init.properties -w '%{response_code}' /1.0/installs/${INSTALL_ID}/properties/cli | httpStatusCheck ||  failc
        INIT_PROPERTIES=$(cat ${TEMPORARY_WORKING_DIRECTORY}/init.properties || failc)
        sleepTime=5
    done


    cp ${TEMPORARY_WORKING_DIRECTORY}/init.properties ${INIT_PROPERTIES_FILE} || failc
}

function awaitUsbDriveRemoval() {
    step "Please remove the USB Drive and place it on top of the BioBrain" true

    sleepTime=0

    while [[ "$DISK_USB" != "" ]]; do
        sleep ${sleepTime}
        detectUsbDriveInternal
        sleepTime=1
    done

    log "USB Drive Removed"
}

function copyBioBrainSoftwareToInternalDrive() {
    step "Copying BioBrain Software to Internal Drive"
    cp ${TEMPORARY_WORKING_DIRECTORY}/biohitech-biobrain.tar.gz ${DISK_INTERNAL_TEMP} || failc

}
function copyInitPropertiesToInternalDrive() {
    step "Copying Init Properties to Internal Drive"

    mkdir -p ${DISK_INTERNAL_TEMP} || failc
    cp ${INIT_PROPERTIES_FILE} ${DISK_INTERNAL_TEMP} || failc
}

function copyInitScriptToInternalDrive() {
    step "Copying Init Script to Internal Drive"

    mkdir -p ${DISK_INTERNAL_TEMP} || failc
    cp ${INIT_SCRIPT} ${DISK_INTERNAL_TEMP} || failc
}

function copyVerifyScriptToInternalDrive() {
    step "Copying Verify Script to Internal Drive"

    mkdir -p ${DISK_INTERNAL_TEMP} || failc
    cp ${VERIFY_SCRIPT} ${DISK_INTERNAL_TEMP} || failc
}

function createLocalInitScript() {
cat <<EOF > ${DISK_INTERNAL_TEMP}/biobrain-init-start.sh || failc
#!/bin/bash

if [[ "\$(tty)" != "/dev/tty1" ]]; then
    exit
fi

FILE=/var/tmp/biobrain/biobrain-init.sh
OUTPUT_DIR=/opt/bht/logs
OUTPUT=\${OUTPUT_DIR}/biobrain-init.log.1
SLEEP_TIME=10

mkdir -p \${OUTPUT_DIR}

if [[ \$? -ne 0 ]]; then
    echo "Unable to create \${OUTPUT_DIR}."
    exit 1
fi

if [[ ! -f \${FILE} ]]; then
    echo "Unable to find Init Script: \$FILE" | tee \${OUTPUT}
fi

echo "Waiting \${SLEEP_TIME} seconds to run biobrain-init.sh. Press CTRL-C to cancel...."
sleep \${SLEEP_TIME}

\${FILE} 2>&1 | tee \${OUTPUT}
EOF

    chmod u+x ${DISK_INTERNAL_TEMP}/biobrain-init-start.sh || failc
    echo "/var/tmp/biobrain/biobrain-init-start.sh" >> ${DISK_INTERNAL_MOUNT}/root/.bashrc || failc
}

function detectExistingMachineKey() {
    log "Detecting existing machine key"

    mountDir=/media/tmp-internal
    mkdir -p "$mountDir" || failc
    detectInternalDrivePartition || (log "Skipping machineKey detection.  Unable to detect internal drive partition." && return)
    mount "/dev/$DISK_INTERNAL_PARTITION" "$mountDir" || (log "Skipping machineKey detection.  Unable to mount internal drive." && return)
    MACHINE_KEY=$( (grep '^machineId=' ${mountDir}/opt/bht/etc/static.properties | cut -d= -f2) || (grep '^machineId=' ${mountDir}/opt/bht/etc/local.properties | cut -d= -f2)  )
    umount ${mountDir}
    rmdir ${mountDir}
}

function detectInternalDrive() {
    log "Detecting Internal Drive"

    DISK_INTERNAL=$(lsblk -b -o SIZE,KNAME,TYPE,SUBSYSTEMS,PKNAME | grep disk | grep -E ':mmc_host:|:scsi:pci([[:space:]]|$)' | sort -n | tail -1 | awk '{print $2}' || failc)

    if [[ "$DISK_INTERNAL" == "" ]]; then
        return 1
    fi

    return 0
}

function doReboot() {
    step "Rebooting"
    reboot || failc
}

function detectInternalDrivePartition() {
    # check for logic volumes first
    DISK_INTERNAL_PARTITION=$(lvs --units b -o lv_size,lv_path,devices | grep "\/${DISK_INTERNAL}" | sort -n | tail -1 | awk '{print $2}' | cut -d/ -f3-)

    if [[ "$DISK_INTERNAL_PARTITION" == "" ]]; then
        DISK_INTERNAL_PARTITION=$(lsblk -b -o SIZE,KNAME,TYPE,SUBSYSTEMS,PKNAME | grep part | grep "\s${DISK_INTERNAL}$" | sort -n | tail -1 | awk '{print $2}' || failc)
    fi

    if [[ "$DISK_INTERNAL_PARTITION" == "" ]]; then
        return 1
    fi

    return 0
}

function detectSerialNumber() {
    step "Detecting Serial Number"

    SERIAL_NUMBER=$(dmidecode -s system-serial-number | grep -v '#' | trim)

    if [[ "$SERIAL_NUMBER" == "" ]]; then
        SERIAL_NUMBER=$(dmidecode -s baseboard-serial-number | grep -v '#' | trim)
    fi

    if [[ "$SERIAL_NUMBER" == "" ]]; then
        return 1
    fi

    log "Detected Serial Number: $SERIAL_NUMBER"
}

function detectUsbDrive() {
    log "Detecting USB Drive"

    sleepTime=0

    while [[ "$DISK_USB" == "" ]]; do
        sleep ${sleepTime}
        detectUsbDriveInternal
        sleepTime=1
    done

    if [[ "$DISK_USB" == "" ]]; then
        return 1
    fi

    return 0
}

function detectUsbDriveInternal() {
    DISK_USB=$(lsblk -b -o SIZE,KNAME,TYPE,SUBSYSTEMS,PKNAME | grep disk | grep ':usb:' | sort -n | tail -1 | awk '{print $2}' || failc)

    if [[ "$DISK_USB" == "" ]]; then
        return 1
    fi

    return 0
}

function detectUsbPartition() {
    DISK_USB_PARTITION=$(lsblk -b -o SIZE,KNAME,TYPE,SUBSYSTEMS,PKNAME | grep part | grep "\s${DISK_USB}$" | sort -n | tail -1 | awk '{print $2}' || failc)

    if [[ "$DISK_USB_PARTITION" == "" ]]; then
        return 1
    fi

    return 0
}

function downloadBioBrainSoftware() {
    step "Downloading Biobrain Software version $BIOBRAIN_SOFTWARE_VERSION"

    version=${BIOBRAIN_SOFTWARE_VERSION}

    log "Downloading Manifest"
    # TODO: insecure
    curlc -o ${TEMPORARY_WORKING_DIRECTORY}/manifest -w '%{response_code}' ${DIGESTER_ADMIN_BASE_URL}/releases/biobrain/${version}/manifest | httpStatusCheck || failc

    log "Downloading tar gz"
    # TODO: insecure
    curlc -o ${TEMPORARY_WORKING_DIRECTORY}/biohitech-biobrain.tar.gz -w '%{response_code}' ${DIGESTER_ADMIN_BASE_URL}/releases/biobrain/${version}/biohitech-biobrain.tar.gz | httpStatusCheck || failc

    log "Checking MD5 Hashes"
    DOWNLOAD_MD5=$(md5sum "${TEMPORARY_WORKING_DIRECTORY}/biohitech-biobrain.tar.gz" | cut -d' ' -f1)
    MANIFEST_MD5=$(cat ${TEMPORARY_WORKING_DIRECTORY}/manifest | grep "biohitech-biobrain.tar.gz" | cut -d' ' -f1)
    rm -f ${TEMPORARY_WORKING_DIRECTORY}/manifest

    if [[ "$DOWNLOAD_MD5" != "$MANIFEST_MD5" ]]; then
        fail "manifest md5 [$MANIFEST_MD5] and biohitech-biobrain.tar.gz [$DOWNLOAD_MD5] do not match."
    fi
}

function downloadInitScript() {
    step "Downloading BioBrain Init Script"

    curlc -o ${INIT_SCRIPT} -w '%{response_code}' ${INIT_SCRIPT_URL} | httpStatusCheck || failc

    chmod +x ${INIT_SCRIPT} || failc
    sync
}

function downloadVerifyScript() {
    step "Downloading BioBrain Init Verify Script"

    curlc -o ${VERIFY_SCRIPT} -w '%{response_code}' ${VERIFY_SCRIPT_URL} | httpStatusCheck || failc

    chmod +x ${VERIFY_SCRIPT} || failc
    sync
}

function downloadMasterImage() {
    step "Downloading Master Image"

    mkdir -p ${MASTER_IMAGE_DIR} || failc

    hash=""

    if [[ -f ${MASTER_IMAGE_FILE} ]]; then
        hash=$(sha256sum ${MASTER_IMAGE_FILE} | awk '{print $1}' || failc)
    fi

    if [[ "$hash" == "$MASTER_IMAGE_HASH" ]]; then
        log "Hashes match. Skipping download."
    else
        log "Requesting new image."
        curlc -H "Accept: application/octet-stream" -H "$API_KEY_HEADER" -o ${MASTER_IMAGE_FILE} -w '%{response_code}' ${MASTER_IMAGE_URL} | httpStatusCheck || failc
    fi
    sync
}

function createInstall() {
    log "Creating Install"

    deployApiCurl -o ${TEMPORARY_WORKING_DIRECTORY}/install.json -w '%{response_code}' --data-binary "{\"usbKey\":$(jsonEncode "$USB_KEY"),\"serialNumber\":$(jsonEncode "$SERIAL_NUMBER"),\"machineKey\":$(jsonEncode "$MACHINE_KEY")}" /1.0/installs | httpStatusCheck ||  failc

    INSTALL_ID=$(cat ${TEMPORARY_WORKING_DIRECTORY}/install.json | jq -r .id || failc)

    if [[ "$INSTALL_ID" == "" ]]; then
        fail "Unable to create Install"
    fi

    log "Found Install $INSTALL_ID"
}

function createTemporaryWorkingDirectory() {
    log "Creating Temporary Working Directory"
    mkdir -p ${TEMPORARY_WORKING_DIRECTORY} || failc
}

function loadInitProperties() {
    log "Loading Init Properties $INIT_PROPERTIES_FILE"
    readProperties ${INIT_PROPERTIES_FILE} || failc
    validateInitProperties || fail "1 or more properties is invalid"
    MASTER_IMAGE_FILE="$MASTER_IMAGE_DIR/$MASTER_IMAGE_TYPE.img.gz"
}

function loadUsbProperties() {
    log "Loading USB Properties $USB_PROPERTIES_FILE"
    readProperties ${USB_PROPERTIES_FILE} || failc
    validateUsbProperties || fail "1 or more properties is invalid"
}

function jsonEncode () {
    printf '%s' "$1" | python -c 'import json,sys; print(json.dumps(sys.stdin.read()))'
}

function mountInternalDrivePartition() {
    log "Mounting Internal Drive"

    mkdir -p "$DISK_INTERNAL_MOUNT" || failc
    mount "/dev/$DISK_INTERNAL_PARTITION" "$DISK_INTERNAL_MOUNT" || failc
}

function mountUsbDrivePartition() {
    log "Mounting USB Drive"

    mkdir -p "$DISK_USB_MOUNT" || failc
    mount "/dev/$DISK_USB_PARTITION" "$DISK_USB_MOUNT" || failc
}

function onFail() {
    message="$1"

    json="{\"status\":$(jsonEncode "${message}")}"

    if [[ "$INSTALL_ID" != "" ]]; then
        deployApiCurl -w '%{response_code}' --data-binary "$json" /1.0/installs/${INSTALL_ID}/fail > /dev/null
    fi
}

function setUsbProperties() {
    step "Setting USB Properties"

    echo "" >> ${INIT_PROPERTIES_FILE}
    echo "USB_KEY=$USB_KEY" >> ${INIT_PROPERTIES_FILE}
}

function startInstall() {
    log "Start Install"
    deployApiCurl -w '%{response_code}' --data-binary "{}" /1.0/installs/${INSTALL_ID}/start | httpStatusCheck || failc
}

function step() {
    message="$1"
    notify="${2:-false}"
    log__ "INFO" ${FUNCNAME[1]} ${BASH_LINENO[0]} "$message"

    json="{\"status\":$(jsonEncode "$message"),\"notify\":$notify}"

    if [[ "$INSTALL_ID" != "" ]]; then
        deployApiCurl -w '%{response_code}' --data-binary "${json}" /1.0/installs/${INSTALL_ID}/status > /dev/null
    fi
}

function validateInitProperties() {
    validateNotEmpty MASTER_IMAGE_URL "$MASTER_IMAGE_URL" || fail "MASTER_IMAGE_URL is blank"
    validateNotEmpty MASTER_IMAGE_HASH "$MASTER_IMAGE_HASH" || fail "MASTER_IMAGE_HASH is blank"
    validateNotEmpty MASTER_IMAGE_TYPE "$MASTER_IMAGE_TYPE" || fail "MASTER_IMAGE_TYPE is blank"
    validateNotEmpty BIOBRAIN_SOFTWARE_VERSION "BIOBRAIN_SOFTWARE_VERSION" || fail "BIOBRAIN_SOFTWARE_VERSION is blank"
}

function validateUsbProperties() {
    validateNotEmpty USB_KEY "${USB_KEY}" || return 1
}

function writeMasterImageToInternalDrive() {
    step "Writing Master Image to Internal Drive"

    gunzip -c ${MASTER_IMAGE_FILE} > /dev/${DISK_INTERNAL} || failc
    partprobe /dev/${DISK_INTERNAL} || failc
    sync
}

function writeNetworkConfigToInternalDrive() {
    step "Writing Network Config to Internal Drive"
    cloudInitFile=${DISK_INTERNAL_MOUNT}/etc/netplan/50-cloud-init.yaml
    cloudInitTmpFile=/tmp/50-cloud-init.yaml

    if [[ -f ${cloudInitFile} ]]; then
        interfaces="$(ifconfig -a | grep -E -o '^en(p|o)[[:digit:]]+(s[[:digit:]]+)?')"
        echo 'network:' > ${cloudInitTmpFile} || failc
        echo '    version: 2' >> ${cloudInitTmpFile} || failc
        echo '    ethernets:' >> ${cloudInitTmpFile} || failc

        for interface in ${interfaces}; do
            echo "        $interface:" >> ${cloudInitTmpFile} || failc
            echo "            dhcp4: true" >> ${cloudInitTmpFile} || failc
            echo "            dhcp6: true" >> ${cloudInitTmpFile} || failc
            echo "            nameservers:" >> ${cloudInitTmpFile} || failc
            echo "                addresses: [8.8.8.8, 8.8.4.4]" >> ${cloudInitTmpFile} || failc
        done

        cp ${cloudInitTmpFile} ${cloudInitFile} || failc
        echo 'network: {config: disabled}' > ${DISK_INTERNAL_MOUNT}/etc/cloud/cloud.cfg.d/99-disable-network-config.cfg || failc
    fi

    # Delete and recreate /etc/machine-id with a unique ID. This affects the IP address received with DHCP networking.
    # Machines with the same ID will receive the same IP address and create IP conflicts.
    rm -f ${DISK_INTERNAL_MOUNT}/etc/machine-id
    dbus-uuidgen --ensure=${DISK_INTERNAL_MOUNT}/etc/machine-id
}

# endregion

# region Utility Functions

function curlc() {
    curl -s -L -H "User-Agent: ${BASH_SOURCE[1]}" "$@"

}

function curlJson() {
    curlc -H 'Content-Type: application/json;charset=UTF-8' -H 'Accept: application/json' "$@"
}

function deployApiCurl() {
    # last argument
    url=$(urlQualify "${@: -1}" ${DEPLOYMENT_API_BASE_URL})

    # remove last argument
    set -- "${@: 1: $#-1}"

    curlJson -H "$API_KEY_HEADER" "$@" ${url}
}

function fail() {
    error="$1"
    func=${2:-${FUNCNAME[1]}}
    line=${3:-${BASH_LINENO[0]}}

    if [[ "$error" != "" ]]; then
        log__ "ERROR" ${func} ${line} ${error}
    fi

    failMessage="Script Failed"

    if [[ "$SCRIPT_FAIL_MESSAGE" != "" ]]; then
        failMessage="$SCRIPT_FAIL_MESSAGE"
    fi

    logf ${failMessage}

    if [[ "$SCRIPT_FAIL_FUNCTION" != "" ]]; then
        ${SCRIPT_FAIL_FUNCTION} "${error}"
    fi

    kill -s TERM $$
    return 1
}

function failc() {
    line=${BASH_LINENO[0]}
    func=$(sed "${line}q;d" ${BASH_SOURCE[0]})
    fail "Command Failed - $func" ${FUNCNAME[1]} ${BASH_LINENO[0]}
}

function httpStatusCheck() {
    validStatusCodes=$1
    statusCode=$2

    if [[ ! -t 0 ]]; then
        statusCode=$(cat -)
    fi

    if [[ "$statusCode" == "" ]]; then
        statusCode="$validStatusCodes"
        validStatusCodes=""
    fi

    if [[ "$validStatusCodes" == "" ]]; then
        validStatusCodes="200"
    fi

    if [[ "$validStatusCodes" != *"$statusCode"* ]]; then
        return 1
    fi

    echo -n ""

    return 0
}

function log__() {
    level=$1
    func=$2
    line=$3
    message="${@:4}"

    if [[ "$level" == "" ]]; then
        level="INFO"
    fi

    if [[ "$__BASENAME" == "" ]]; then
        __BASENAME=$(basename ${BASH_SOURCE[0]})
    fi

    echo "[log] $(date -u +"%Y-%m-%dT%H:%M:%SZ") $level $__BASENAME:$func():$line : $message"
}

function log() {
    log__ "INFO" ${FUNCNAME[1]} ${BASH_LINENO[0]} "$@"
}

function logd() {
    log__ "DEBUG" ${FUNCNAME[1]} ${BASH_LINENO[0]} "$@"
}

function loge() {
    log__ "ERROR" ${FUNCNAME[1]} ${BASH_LINENO[0]} "$@"
}

function logf() {
    log__ "FATAL" ${FUNCNAME[1]} ${BASH_LINENO[0]} "$@"
}

function logi() {
    log__ "INFO" ${FUNCNAME[1]} ${BASH_LINENO[0]} "$@"
}

function logw() {
    log__ "WARN" ${FUNCNAME[1]} ${BASH_LINENO[0]} "$@"
}

function pass() {
    passMessage="Script Finished"

    if [[ "$SCRIPT_PASS_MESSAGE" != "" ]]; then
        passMessage="$SCRIPT_PASS_MESSAGE"
    fi

    logi ${passMessage}

    if [[ "$SCRIPT_PASS_FUNCTION" != "" ]]; then
        ${SCRIPT_PASS_FUNCTION} ${passMessage}
    fi

    return 0
}


function readProperties() {
    if [[ -f "$1" ]]; then
        while IFS='=' read -r key value; do
            key=$(echo ${key} | tr '.' '_')

            if [[ "$key" != "" ]]; then
                eval ${key}=\$value
            fi
        done < "$1"
    else
        return 1
    fi
}

function removeUdevNetRules() {
    file="$DISK_INTERNAL_MOUNT/etc/udev/rules.d/70-persistent-net.rules"

    if [[ -f "$file" ]]; then
		log "Deleting 70-persistent-net.rules..."
        rm -f "$file" || failc
	fi
}

function rootCheck() {
    step "Performing Root Check"

    if [[ "$EUID" -ne 0 ]]; then
        fail "This script must be run as root"
    fi
}

function scriptInit() {
    trap 'exit 1' TERM
}

function trim() {
    value="$1"

    if [[ ! -t 0 ]]; then
        value=$(cat -)
    fi

    echo -n "$(echo "$value" | sed -E 's/^[[:space:]]*//g' |  sed -E 's/[[:space:]]*$//g')"
}

function validateNotEmpty() {
    name=$1
    value=$2

    if [[ "$value" == "" ]]; then
        log__ "ERROR" ${FUNCNAME[1]} ${BASH_LINENO[0]} "$name is empty"
        return 1
    fi
}

function unmountPartitions() {
    echo "Unmounting existing partitions"
    mounts=$(lsblk | grep -v initramfs | grep -E '\s+part\s+[^\s]' | awk '{print $NF}')

    for mount in ${mounts}; do
        echo "Unmounting $mount"
        umount "$mount" || fail "Unable to unmount $mount"
    done
}

function urlQualify() {
    url=$1
    base=$2

    if [[ "${url:0:1}" == "/" ]]; then
        url="$base$url"
    fi

    echo -n ${url}
}

# endregion

main "$@"
