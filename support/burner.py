#!/usr/bin/python
__author__="Austin Barrett"

import getpass
import subprocess
import time

print "   (                           "
print " ( )\   (  (           (  (    "
print " )((_) ))\ )(   (     ))\ )(   "
print "((_)_ /((_|()\  )\ ) /((_|()\  "
print " | _ |_))( ((_)_(_/((_))  ((_) "
print " | _ \ || | '_| ' \)) -_)| '_| "
print " |___/\_,_|_| |_||_|\___||_|   "
print "================================"

if getpass.getuser() != 'root':
	print "You must be root to run this script."
	exit(1)

lscmd="find /dev -maxdepth 1 -type b | sort | grep '^/dev/sd'"

# Confirms no SSDs are active
while True:
	raw_input("Please make sure both power lights on the clone are turned off.  Press Enter when ready...")
	disk0 = str(subprocess.Popen(lscmd, shell=True, stdout=subprocess.PIPE).communicate()[0])

        if disk0 == '':
		break
	print "At least one of the blue power lights are on."

raw_input("Please turn on the Power for the Master SSD.  Press Enter to continue...");

attempt = 0
maxAttempts = 10

# Gets the Master SSD
masterDisk = '' 
while masterDisk == '' and attempt < maxAttempts:
	attempt = attempt + 1
        print "Detecting Master SSD (attempt " + str(attempt) + ")"
	masterDisk = str(subprocess.Popen(lscmd + ' | head -1', shell=True , stdout=subprocess.PIPE).communicate()[0]).strip()
	time.sleep(.5)

if masterDisk == '':
	print "Unable to detect Master SSD"
	exit(1)

print "masterDisk: [" +  masterDisk + "]"

raw_input("Please turn on the Power for the Clone SSD.  Press Enter to continue...");
attempt = 0

# Gets Clone SSD
cloneDisk = ''
while (cloneDisk == '' or cloneDisk == masterDisk) and attempt < maxAttempts:
        attempt = attempt + 1
        print "Detecting Clone SSD (attempt " + str(attempt) + ")"
	cloneDisk = str(subprocess.Popen(lscmd + " | grep -v '" + masterDisk + "' | head -1", shell=True , stdout=subprocess.PIPE).communicate()[0]).strip()
	time.sleep(.5)

if cloneDisk == '' or cloneDisk == masterDisk:
	print "Unable to detect Clone SSD"
        exit(1)

print "cloneDisk: [" + cloneDisk + "]"

# Confirms whether or not to run the command
ddcmd = "dd if=" + masterDisk + " of=" + cloneDisk + " bs=4096"

while True:
	yn = raw_input("Would you like to run this command?\n\n" + ddcmd + " [y/n] ").lower()
	if yn == "y":
		break
	elif yn == "n":
		print "Exiting..."
		exit(0)
	else:
		print "Please select (y)es or (n)o!"

print "Running..."
result=str(subprocess.Popen(ddcmd, shell=True, stderr=subprocess.STDOUT, stdout=subprocess.PIPE).communicate()[0]);
print "Done: " + result
