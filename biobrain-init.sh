#!/bin/bash

###############################################################################
# Prepares a BioBrain for deployment
###############################################################################

# region Global Variables

API_BASE_URL=https://api.biohitechcloud.com/internal
API_KEY=dd7a0d38-1a8d-45b2-8f73-9d8aaa683447
API_KEY_HEADER="x-biohitech-auth-token: $API_KEY"

DEPLOYMENT_BASE_URL=https://deploy.biohitechcloud.com
DEPLOYMENT_API_BASE_URL=https://deploy.biohitechcloud.com/api
DIGESTER_ADMIN_BASE_URL=https://digester-admin.biohitechcloud.com
ETHERNET_ADAPTER_INTERFACE=""
ETHERNET_ADAPTER_MAC=""
LOG_FILE=/opt/bht/logs/biobrain-init.log
OS_VERSION=$(lsb_release -a 2>/dev/null | grep ^Release: | awk '{print $2}')
OS_VERSION_MAJOR=$(echo "$OS_VERSION" | cut -d. -f1)
OS_VERSION_MINOR=$(echo "$OS_VERSION" | cut -d. -f2)
UBUNTU_18_OR_NEWER=$(expr "$OS_VERSION_MAJOR" '>=' '18')
UBUNTU_12_OR_OLDER=$(expr "$OS_VERSION_MAJOR" '<=' '12')

TEMPORARY_WORKING_DIRECTORY=/var/tmp/biobrain
PROPERTIES_FILE=${TEMPORARY_WORKING_DIRECTORY}/init.properties

SCRIPT_FAIL_FUNCTION='onFail'
SCRIPT_FAIL_MESSAGE='BioBrain Initialization Failed'
SCRIPT_PASS_FUNCTION='onPass'
SCRIPT_PASS_MESSAGE='BioBrain Initialized'

# Detected Variables
MAC_ADDRESS=""
SERIAL_NUMBER=""


# Supplied Variables
BIOBRAIN_SOFTWARE_VERSION=""
DEVICE_TYPE=""
HARDWARE_MODEL=""
HARDWARE_VENDOR=""
INSTALL_ID=""
MACHINE_KEY=""
ORDER_NUMBER=""
ORDER_VENDOR=""
ROLE_ID=""

# Validation Variables
VALID_DEVICE_TYPES="ab,kiosk,masterk,scale,xgb"
VALID_HARDWARE_MODELS="BOXNUC5CPYH,CL210,DCCP847DYE"
VALID_HARDWARE_VENDORS="Intel,Logic Supply"
VALID_ORDER_VENDORS="Amazon,GDC,Logic Supply"
VALID_ROLE_IDS="development,machine,scale,signage"

# endregion

# region Script Functions

function main() {
    if [[ "$(tty)" != "/dev/tty1" ]]; then
        exit
    fi

    scriptInit
    log "Initializing BioBrain"
    rootCheck
    createTemporaryWorkingDirectory
    waitForNetwork
    loadInitProperties
    detectSerialNumber
    detectSensors
    expandFileSystem
    configureFileSystemLayout
    installBioBrainSoftware
    regenerateHostKeys
    configureNetwork
    configureSalt
    disableInternetChatter
    disableInteractiveFsck
    enableFsckAtBoot
    configureTerminal
    configureGrub
    configurePropertiesFiles
    configureTime
    configureSudo
    configureSerialPorts
    configureJobs
    configureVerifyScript
    updateCloud
    getApiKey
    clearExistingStagedUpdates
    disableAutomaticLogin
    configurePermissions
    sync || failc
    cleanup
    pass
}

function cleanup() {
    step "Performing Cleanup"

    if [[ -f ~/image-version ]]; then
        rm ~/image-version
    fi

    if [[ -f ~/init-biobrain.sh ]]; then
        rm ~/init-biobrain.sh
    fi

    log "Copy Init Properties to Install"
    cp ${PROPERTIES_FILE} /opt/bht/install

    log "Copy This script Install"
    cp ${BASH_SOURCE[0]} /opt/bht/install

    log "Removing Auto-Run Script"
    sed -i -E 's/^\/var\/tmp\/biobrain\/biobrain-init-start.sh$//g' ~/.bashrc

    log "Removing Temporary Working Directory"
    rm -rf ${TEMPORARY_WORKING_DIRECTORY}
}

function clearExistingStagedUpdates() {
    step "Clearing Existing Staged Updates From Deployment Server"

    # TODO: insecure
    curlc -X POST ${DEPLOYMENT_BASE_URL}/init/${MACHINE_KEY}/cleanup || failc
}
function compareVersions {
    if [[ $1 == $2 ]]
    then
        return 0
    fi
    local IFS=.
    local i ver1=($1) ver2=($2)
    # fill empty fields in ver1 with zeros
    for ((i=${#ver1[@]}; i<${#ver2[@]}; i++))
    do
        ver1[i]=0
    done
    for ((i=0; i<${#ver1[@]}; i++))
    do
        if [[ -z ${ver2[i]} ]]
        then
            # fill empty fields in ver2 with zeros
            ver2[i]=0
        fi
        if ((10#${ver1[i]} > 10#${ver2[i]}))
        then
            return 1
        fi
        if ((10#${ver1[i]} < 10#${ver2[i]}))
        then
            return -1
        fi
    done
    return 0
}

function configureJobs {
    step "Configuring Jobs"

    log "Installing root cron"

    echo "0 */4 * * * /opt/bht/biobrain/bin/sleepupto.sh 600 && /bin/su - -c 'service salt-minion restart' > /dev/null 2>&1" >> /var/spool/cron/crontabs/root || failc
    chmod 600 /var/spool/cron/crontabs/root || failc

    log "Installing bht cron jobs"

    cp /opt/bht/biobrain/etc/crontab.example /var/spool/cron/crontabs/bht || failc

    chown bht:bht /var/spool/cron/crontabs/bht || failc

    if [[ "$UBUNTU_18_OR_NEWER" == "1" ]]; then
        rebootCmd=$(cat /opt/bht/biobrain/etc/crontab.example | grep -v '#' | grep 'biobrain\.sh' | head -1 | awk '{$1=$2=$3=$4=$5=""; print $0}' | sed -E 's/^[[:space:]]*//g' || failc)
        echo "" >> /var/spool/cron/crontabs/bht || failc
        echo "@reboot $rebootCmd" >> /var/spool/cron/crontabs/bht || failc
    else
        ln -sf /opt/bht/biobrain/etc/init/biobrain.conf /etc/init/biobrain.conf || failc
        chmod 744 /etc/init/biobrain.conf || failc
    fi

    chmod 600  /var/spool/cron/crontabs/bht || failc
}

function configureDhcp() {
    step "Configuring DHCP"
    log "Setting DNCP Client timeout parameters"

    echo "append domain-name-servers 8.8.8.8, 8.8.4.4;" >> /etc/dhcp/dhclient.conf || failc
    echo "timeout 60;" >> /etc/dhcp/dhclient.conf || failc
    echo "retry 15;" >> /etc/dhcp/dhclient.conf || failc
}


function expandFileSystem() {
    step "Expanding Root Partition and File System"

    disk=$(fdisk -l | grep Disk | grep -oP '/dev/(sda|mmcblk1)(?=:)')
    partition=$((df --type=ext4; df --type=ext2) | grep -oP "^${disk}[a-z0-9]+")

    if [[ "$disk" == "" ]]; then
        fail "Could not detect disk"
    fi

    if [[ "$partition" == "" ]]; then
        fail "Could not detect partition"
    fi

    growpart "$disk" 2
    resize2fs "$partition"
}


function configureFileSystemLayout() {
    log "Configuring File System Layout"

    mkdir -p /etc/salt/minion.d || failc
    mkdir -p /opt/bht || failc
    mkdir -p /opt/bht/install || failc
    mkdir -p /opt/bht/bin || failc
    mkdir -p /opt/bht/etc || failc
    mkdir -p /opt/bht/etc/salt/minion || failc
    mkdir -p /opt/bht/etc/networks || failc
    mkdir -p /opt/bht/var || failc
    mkdir -p /opt/bht/var/software || failc
    mkdir -p /opt/bht/var/software/staged || failc
    mkdir -p /opt/bht/var/software/installed || failc
    mkdir -p /opt/bht/biobrain || failc
    mkdir -p /opt/bht/logs || failc
    mkdir -p /opt/bht/events || failc

    touch /opt/bht/logs/script.log || failc
}

function configureGrub {
step "Configuring Grub"

cat << EOF >> /etc/default/grub || failc


# Make sure unit boots automatically after a failure
GRUB_RECORDFAIL_TIMEOUT=5
EOF

    sed -i -E 's/^(GRUB_CMDLINE_LINUX_DEFAULT=").*?"$/\1 systemd.gpt_auto=0"/g' /etc/default/grub || failc
    update-grub || failc
}

function configureHostname() {
    step "Changing Hostname to $MACHINE_KEY"
    echo "$MACHINE_KEY" > /etc/hostname || failc
    echo "127.0.0.1  $MACHINE_KEY" >> /etc/hosts || failc

    if [[ "$UBUNTU_18_OR_NEWER" == "1" ]]; then
        hostnamectl set-hostname "$MACHINE_KEY" || failc
    else
        service hostname start || failc
    fi
}

function configurePropertiesFiles {
    step "Configuring Properties Files"
    version=${BIOBRAIN_SOFTWARE_VERSION}

    if [[ "$version" == "latest" ]]; then
        configurePropertiesFilesCurrent
        return 0
    fi

    majorVersion=$(echo "$version" | cut -d. -f1)
    baseVersion='8.0.0'


    if (( "$majorVersion" <= "6" )); then
        baseVersion='6.5.8'
    fi

    compareVersions ${version} ${baseVersion}

    if (( "$?" >= "0" )); then
        configurePropertiesFilesCurrent
    else
        configurePropertiesFilesLegacy
    fi
}

function configurePropertiesFilesCurrent() {
    configurePropertiesFilesCommon /opt/bht/etc/static.properties /opt/bht/etc/local.properties
    # TODO: wifi config if allen bradley
}

function configurePropertiesFilesLegacy() {
    configurePropertiesFilesCommon /opt/bht/etc/local.properties /opt/bht/etc/local.properties
}

function configurePropertiesFilesCommon() {
    staticProperties=$1
    localProperties=$2

    log "Creating $staticProperties"
    touch ${staticProperties} || failc

    log "Creating $localProperties"
    touch ${localProperties} || failc

    mainModule="$DEVICE_TYPE"
    extraModules="remote-control"

    if [[ "$mainModule" == "ab" ]]; then
        mainModule="allen-bradley"
    elif [[ "$mainModule" == "kiosk" ]]; then
        mainModule="signage"
    elif [[ "$mainModule" == "scale" ]]; then
        mainModule="baler"
    fi

    echo "machineId=$MACHINE_KEY" >> ${staticProperties} || failc
    echo "modules=$mainModule,$extraModules" >> ${localProperties} || failc
}

function configureNetwork() {
    step "Configuring Network"

    detectMacAddress
    configureHostname
    configureNetworkInterfaces
    configureIpTables
    configureDhcp
}

function configureIpTables() {
    step "Configuring IP Tables"


    if [[ "$UBUNTU_18_OR_NEWER" == "1" ]]; then
        configureIpTablesNew
    else
        configureIpTablesOld
    fi
}

function configureIpTablesNew() {
cat << EOF > /etc/network/if-pre-up.d/iptableload || failc
#!/bin/sh
cat /opt/bht/biobrain/etc/iptables.bht.* | iptables-restore -n
exit 0
EOF

chmod +x /etc/network/if-pre-up.d/iptableload || failc

cat << EOF > /etc/systemd/system/bht.iptables.service || failc
[Unit]
Description=Apply base firewall rules for bht services

[Service]
Type=oneshot
ExecStart=/etc/network/if-pre-up.d/iptableload

[Install]
WantedBy=network-pre.target
EOF

systemctl enable bht.iptables.service || failc
}


function configureIpTablesOld() {
    echo -n ""
}

function configureNetworkInterfaces() {
    step "Configuring Network Interfaces"

    if [[ "$UBUNTU_18_OR_NEWER" == "1" ]]; then
        configureNetworkInterfacesNew
    else
        configureNetworkInterfacesOld
    fi
}

function configureNetworkInterfacesNew() {
    if [[ -f /etc/netplan/50-cloud-init.yaml ]]; then
        rm /etc/netplan/50-cloud-init.yaml || failc
    fi

    # Logic Supply interfaces show up as enp0s0. NUC interface shows up as eno1
    internetInterface=$(ifconfig -a | grep -E -o '^enp[[:digit:]]+s[[:digit:]]+' | sed -E 's/^(enp([[:digit:]]+)s[[:digit:]]+)/\2 \1/g' | sort -n | tail -1 | awk '{print $2}' || failc)
    if [[ "$internetInterface" == "" ]]; then
        internetInterface=$(ifconfig -a | grep -E -o '^eno[[:digit:]]+' | sed -E 's/^(eno([[:digit:]]+))/\2 \1/g' | sort -n | tail -1 | awk '{print $2}' || failc)
    fi

    if [[ "$internetInterface" == "" ]]; then
        fail "Unable to detect interface interface"
    fi

    if [[ "$DEVICE_TYPE" == "ab" ]] || [[ "$DEVICE_TYPE" == "xgb" ]]; then
        # Logic Supply interfaces show up as enp0s1. NUC PLC interface (dongle) shows up as enx followed by the mac address.
        plcInterface=$(ifconfig -a | grep -E -o '^enx[0-9a-f]+' | sed -E 's/^(enx([0-9a-f]+))/\2 \1/g' | sort -n | tail -1 | awk '{print $2}' || failc)
        if [[ "$plcInterface" == "" ]]; then
            plcInterface=$(ifconfig -a | grep -E -o '^enp[[:digit:]]+s[[:digit:]]+' | sed -E 's/^(enp([[:digit:]]+)s[[:digit:]]+)/\2 \1/g' | sort -n | head -1 | awk '{print $2}' || failc)
        fi

        if [[ "$plcInterface" == "" ]]; then
            fail "Unable to detect plc interface"
        fi

        if [[ "$internetInterface" == "$plcInterface" ]]; then
            fail "Internet and Plc interfaces are the same: $internetInterface"
        fi
    fi


cat <<EOF > /etc/netplan/99-bht-cellular.yaml || failc
network:
    version: 2
    ethernets:
        wwp0s21f0u8i12:
            addresses: []
            dhcp4: false
            dhcp6: false
            nameservers:
                addresses: [8.8.8.8, 8.8.4.4]
EOF

cat <<EOF > /etc/netplan/99-bht-internet.yaml || failc
network:
    version: 2
    ethernets:
        ${internetInterface}:
            addresses: []
            dhcp4: true
            dhcp6: true
            nameservers:
                addresses: [8.8.8.8, 8.8.4.4]
EOF

if [[ "$DEVICE_TYPE" == "ab" ]] || [[ "$DEVICE_TYPE" == "xgb" ]]; then
    if [[ "$plcInterface" =~ ^enx[0-9a-f]+ ]]; then
cat <<EOF > /etc/netplan/99-bht-plc.yaml || failc
network:
    version: 2
    ethernets:
        bhtdev0:
            match:
                name: enx*
            addresses: [25.25.25.200/24]
EOF
    else
cat <<EOF > /etc/netplan/99-bht-plc.yaml || failc
network:
    version: 2
    ethernets:
        ${plcInterface}:
            addresses: [25.25.25.200/24]
EOF
    fi
fi
}

function configureNetworkInterfacesOld() {
    echo "  pre-up iptables-restore < /opt/bht/biobrain/etc/iptables.bht.rules" >> /etc/network/interfaces

    if [[ "$DEVICE_TYPE" == "ab" ]] || [[ "$DEVICE_TYPE" == "xgb" ]]; then
        detectEthernetAdapter

        if [[ "$ETHERNET_ADAPTER_INTERFACE" != "" ]]; then
            sed -i "s/auto $ETHERNET_ADAPTER_INTERFACE/ /g" /etc/network/interfaces || failc
            sed -i "s/iface $ETHERNET_ADAPTER_INTERFACE inet dhcp/ /g" /etc/network/interfaces || failc
        fi

        ETHERNET_ADAPTER_INTERFACE="eth1"
        echo "SUBSYSTEM==\"net\", ACTION==\"add\", DRIVERS==\"?*\", ATTR{address}==\"$ETHERNET_ADAPTER_MAC\", ATTR{dev_id}==\"0x0\", ATTR{type}==\"1\", KERNEL==\"eth*\", NAME=\"$ETHERNET_ADAPTER_INTERFACE\""  > /etc/udev/rules.d/90-bht-net.rules || failc
        echo "auto $ETHERNET_ADAPTER_INTERFACE" >> /etc/network/interfaces || failc
        echo "iface $ETHERNET_ADAPTER_INTERFACE inet static" >> /etc/network/interfaces || failc
        echo "  address 25.25.25.200" >> /etc/network/interfaces || failc
        echo "  netmask 255.255.255.0" >> /etc/network/interfaces || failc
    fi
}

function configurePermissions() {
    step "Configuring Permissions for /opt/bht"
    chown -R "bht:bht" "/opt/bht" || failc
}

function configureSalt() {
    step "Configuring Salt minion"

    echo "id:  $MACHINE_KEY" > /opt/bht/etc/salt/minion/bht-local.conf || failc

cat <<EOF > /opt/bht/etc/salt/minion/bht-remote.conf || failc
master: digester-salt-master.biohitechcloud.com
master_port: 443
grains:
  roles:
    - biobrain
    - nuc
auth_timeout: 60
random_reauth_delay: 60
recon_default: 1000
recon_max: 60000
recon_randomize: True
EOF

    ln -s /opt/bht/etc/salt/minion/bht-local.conf /etc/salt/minion.d/01-bht-local.conf || failc
    ln -s /opt/bht/etc/salt/minion/bht-remote.conf /etc/salt/minion.d/02-bht-remote.conf || failc
    chmod 660 /opt/bht/etc/salt/minion/bht-local.conf || failc
    chmod 660 /opt/bht/etc/salt/minion/bht-remote.conf || failc

    log "Removing Old Salt Key"
    sshCloud digester-salt-master-prod.bht "sudo salt-key -d ${MACHINE_KEY} -y" || failc

    log "Restarting Salt Minion"
    service salt-minion restart || failc

    log "Adding New Salt Key"
    sshCloud digester-salt-master-prod.bht "sudo ./machineKey.sh ${MACHINE_KEY} " || failc

    log "Running highstate"
    salt-call state.highstate || failc
}

function configureSerialPorts() {
    step "Giving permissions to tty serial ports"

    if [[ -e /dev/ttyUSB0 ]]; then
        chmod 777 /dev/ttyUSB0 || failc
    fi

    if [[ -e /dev/ttyUSB1 ]]; then
        chmod 777 /dev/ttyUSB1 || failc
    fi
}

function configureSudo {
    step "Setting up sudo permissions for bht"
cat <<EOF > /etc/sudoers.d/bht || failc
bht  ALL= NOPASSWD: /opt/bht/biobrain/bin/run.sh
bht  ALL= NOPASSWD: /opt/bht/biobrain/bin/hid-linux
bht  ALL= NOPASSWD: /bin/date
bht  ALL= NOPASSWD: /bin/chmod
bht  ALL= NOPASSWD: /tmp/script.sh
bht  ALL= NOPASSWD: /opt/bht/biobrain/bin/net-config-static.sh
bht  ALL= NOPASSWD: /opt/bht/biobrain/bin/net-config-dhcp.sh
bht  ALL= NOPASSWD: /opt/bht/biobrain/bin/check-temp.sh
Defaults visiblepw
EOF

    chmod 0440 /etc/sudoers.d/* || failc
}

function configureTerminal() {
    step "Configuring Terminal"

cat << EOF >> /home/bht/.bashrc || failc

export PATH="\$PATH:/home/bht/bin:/opt/bht/bin:/opt/bht/biobrain/bin"
export LS_COLORS="\$LS_COLORS:di=0;36:"
export PS1='\[\e]0;\u@\h: \w\a\]\${debian_chroot:+(\$debian_chroot)}\[\033[00;32m\]\u@\h\[\033[00m\]:\[\033[00;36m\]\w\[\033[00m\]\$ '
EOF

if [[ "$UBUNTU_12_OR_OLDER" == "1" ]]; then
cat << EOF >> /home/bht/.bashrc || failc
export PS1='\u@\h:\w\$ '
EOF
fi

echo ':highlight Comment ctermfg=cyan' >> /home/bht.vimrc || failc
}

function configureTime() {
    step "Configuring Time Zone"

    log "Setting localtime to UTC"
    ln -sf /usr/share/zoneinfo/UTC /etc/localtime || failc

    log "Performing a time sync"
    if [[ -f /tmp/timesync ]]; then
        rm /tmp/timesync || failc
    fi

    /opt/bht/biobrain/bin/timesync.sh || failc

    log "Updating hardware clock"
    hwclock -w || failc
}

function copyUdevRules() {
    step "Copying Udev Rules"

    tmpdir=${TEMPORARY_WORKING_DIRECTORY}/udev

    mkdir ${tmpdir} || failc
    cp /opt/bht/biobrain/etc/udev/rules.d/99-*.rules ${tmpdir} || failc

    if [[ "$UBUNTU_18_OR_NEWER" == "1" ]]; then
        # Network config is handled by netplan
        rm ${tmpdir}/99-bht-devices.rules || failc
    fi

    cp ${tmpdir}/*.rules /etc/udev/rules.d || failc
}


function createTemporaryWorkingDirectory() {
    step "Creating Temporary Working Directory"
    mkdir -p ${TEMPORARY_WORKING_DIRECTORY} || failc
}

function detectEthernetAdapter() {
    detectEthernetAdapterInternal

    if [[ "$ETHERNET_ADAPTER_MAC" == "" ]]; then
        step "Please connect the USB ethernet adapter" true
    fi

    while [[ "$ETHERNET_ADAPTER_MAC" == "" ]]; do
        sleep 3
        detectEthernetAdapterInternal
    done
}

function detectEthernetAdapterInternal() {
    interfaces=$(lshw -C network | sed -E 's/^[[:space:]]+\*-network.*/|/g' | sed -E 's/^\s+//g')
    OLD_IFS="$IFS"
    IFS="|"

    for interface in ${interfaces}; do
        usb=$(echo "$interface" | grep -i '\susb')

        if [[ "$usb" != "" ]]; then
            ETHERNET_ADAPTER_MAC=$(echo "$interface"  | grep '^serial:' | awk '{print $2}')
            ETHERNET_ADAPTER_INTERFACE=$(echo "$interface"  | grep '^logical name:' | awk '{print $2}')
            break
        fi
    done

    IFS="$OLD_IFS"
}


function detectMacAddress() {
    step "Detecting MAC Address"

    if [[ "$UBUNTU_18_OR_NEWER" == "1" ]]; then
        MAC_ADDRESS=$(ifconfig | grep -i ether | egrep -io "((\d|\w){2}:){5}(\w|\d){2}" | head -1)
    else
        macFilter='b827eb|F44D30|ECA86B|C89CDC|C03FD5|B8AEED|7427EA|4487FC|1078D2|002511|002197|001E90|001BB9|001921|0016EC|00142A|00115B|001035|000D87|000AE6|000795|94C691'
        MAC_ADDRESS=$(ifconfig -a | sed 's/://g' | egrep -i "$macFilter" | awk '{print $NF}' | awk '{print toupper($0)}')
    fi

    if [[ -z "$MAC_ADDRESS" ]] || [[ "$MAC_ADDRESS" == "" ]]; then
        fail "Unable to find a mac address"
    fi

    log "Detected MAC Address: $MAC_ADDRESS"
}

function detectSerialNumber() {
    step "Detecting Serial Number"

    SERIAL_NUMBER=$(dmidecode -s system-serial-number | grep -v '#' | trim)

    if [[ "$SERIAL_NUMBER" == "" ]]; then
        SERIAL_NUMBER=$(dmidecode -s baseboard-serial-number | grep -v '#' | trim)
    fi

    if [[ "$SERIAL_NUMBER" == "" ]]; then
        return 1
    fi

    log "Detected Serial Number: $SERIAL_NUMBER"
}

function detectSensors() {
    step "Detecting Sensors"
    yes "" | sensors-detect --auto || failc
}

function disableAutomaticLogin() {
    step "Disabling Automatic Login"

    if [[ "$UBUNTU_18_OR_NEWER" == "1" ]]; then
        cp /lib/systemd/system/getty@.service.orig /lib/systemd/system/getty@.service || failc
    else
        cp /etc/init/tty1.conf.orig /etc/init/tty1.conf
    fi
}

function disableInteractiveFsck() {
    step "Disabling Interactive Fsck"

    fsckFile=/etc/default/rcS

    if [[ "$UBUNTU_18_OR_NEWER" == "1" ]]; then
        fsckFile=/lib/init/vars.sh
    fi

    sed -i 's/FSCKFIX=no/FSCKFIX=yes/g' ${fsckFile} || failc
}

function enableFsckAtBoot() {
    step "Enabling e2fsck Command to Run on FS Mount at Boot"
    tune2fs -c 1 /dev/mmcblk1p2
}

function disableInternetChatter() {
    # only disabling for versions < 18 because, 18 images already have this disabled

    if [[ "$UBUNTU_18_OR_NEWER" == "0" ]]; then
        step "Disabling Automatic Updates"

        sed -i 's/APT::Periodic::Update-Package-Lists "1";/APT::Periodic::Update-Package-Lists "0";/g' /etc/apt/apt.conf.d/10periodic || failc

        log "Removing Whoopsie"
        update-rc.d -f whoopsie remove
    fi
}

function getApiKey() {
    step "Retrieving API Token"
    token=$(deployCurl -o ${TEMPORARY_WORKING_DIRECTORY}/apiKeyJson -X POST -w '%{response_code}' /init/${MACHINE_KEY}/accessToken | httpStatusCheck || failc)
    tokenKey=$(cat  "${TEMPORARY_WORKING_DIRECTORY}/apiKeyJson" | jq -r .tokenKey || failc)
    echo "api.key=${tokenKey}" >> /opt/bht/etc/local.properties || failc
}

function installBioBrainSoftware() {
    step "Installing BioBrain Software"
    pushd /opt/bht/biobrain/ || failc
    tar zxvf ${TEMPORARY_WORKING_DIRECTORY}/biohitech-biobrain.tar.gz || failc
    popd
    sync
}


function loadInitProperties() {
    step "Loading Properties $PROPERTIES_FILE"
    readProperties ${PROPERTIES_FILE} || failc
    validateInitProperties || fail "1 or more properties is invalid"
}

function onFail() {
    message="$1"

    json="{\"status\":$(jsonEncode "${message}")}"

    if [[ "$INSTALL_ID" != "" ]]; then
        deployApiCurl -w '%{response_code}' --data-binary "$json" /1.0/installs/${INSTALL_ID}/fail > /dev/null
    fi
}

function onPass() {
    message="$1"

    json="{\"status\":$(jsonEncode "${message}")}"

    if [[ "$INSTALL_ID" != "" ]]; then
        deployApiCurl -w '%{response_code}' --data-binary "$json" /1.0/installs/${INSTALL_ID}/finish > /dev/null
    fi

    log "Rebooting..."

    reboot
}

function regenerateHostKeys {
    step "Regenerating Host Keys"
    rm /etc/ssh/ssh_host_*
    fuser -v /var/cache/debconf/config.dat
    count=0

    while (( $count < 30 )); do
        dpkg-reconfigure openssh-server

        if (( $? == 0 )); then
            break
        fi

        count=$(( $count + 1 ))
        sleep 1
    done

}

function configureVerifyScript() {
    step "Configuring Verify Script"

    scriptName='/opt/bht/bin/verify.sh'

    cp "$TEMPORARY_WORKING_DIRECTORY/biobrain-init-verify.sh" /opt/bht/bin || failc

cat << EOF >> ${scriptName} || failc
#!/bin/bash

/opt/bht/bin/biobrain-init-verify.sh 2>&1 | tee /opt/bht/logs/biobrain-init-verify.log.1

if [[ "\$?" == "0" ]]; then
    /opt/bht/biobrain/bin/uploadlogs.sh
fi
EOF

chmod +x ${scriptName} || failc

cat << EOF >> /home/bht/.bashrc || failc


if [[ ! -f /opt/bht/etc/BIOBRAIN_VERIFIED ]]; then
    echo ""
    echo "--------------------------------------------------------------------------------"
    echo "This BioBrain has not yet been verified.  To verify, please type:"
    echo ""
    echo "sudo ${scriptName}"
    echo "--------------------------------------------------------------------------------"
    echo ""
fi
EOF

}
function updateCloud () {
    step "Updating Cloud Information"
    apiCurl -o ${TEMPORARY_WORKING_DIRECTORY}/biobrainJson -X GET -w '%{response_code}' /v1/biobrains/${MACHINE_KEY} | httpStatusCheck 200,404 || failc
    biobrainJson=$(cat ${TEMPORARY_WORKING_DIRECTORY}/biobrainJson || failc)

    if [[ "$biobrainJson" == "" ]]; then
        biobrainJson="{}"
    fi

    biobrainId=$(echo "${biobrainJson}" |  jq -r '.id//""' || failc)

    updateUrl=/v1/biobrains
    updateMethod=POST

    if [[ "$biobrainId" == "" ]]; then
        log "No BioBrain Found in the cloud, assuming new"
        # reset biobrain json in case we received a 404 error json response
        biobrainJson="{}"
    else
        log "Found existing BioBrain in cloud: $biobrainJson"
        updateUrl="$updateUrl/$biobrainId"
        updateMethod=PUT
    fi

    biobrainJson=$(echo "$biobrainJson" | jq ".buildDate = \"$(date +%Y-%m-%d)\"" || failc)
    biobrainJson=$(echo "$biobrainJson" | jq ".buildTypeId = \"$DEVICE_TYPE\"" || failc)
    biobrainJson=$(echo "$biobrainJson" | jq ".description=(.description // \"\")" || failc)
    biobrainJson=$(echo "$biobrainJson" | jq ".hardwareModel = \"$HARDWARE_MODEL\"" || failc)
    biobrainJson=$(echo "$biobrainJson" | jq ".hardwareVendor = \"$HARDWARE_VENDOR\"" || failc)
    biobrainJson=$(echo "$biobrainJson" | jq ".macAddress = \"$MAC_ADDRESS\"" || failc)
    biobrainJson=$(echo "$biobrainJson" | jq ".machineKey = \"$MACHINE_KEY\"" || failc)
    biobrainJson=$(echo "$biobrainJson" | jq ".orderNumber = \"$ORDER_NUMBER\"" || failc)
    biobrainJson=$(echo "$biobrainJson" | jq ".orderVendor = \"$ORDER_VENDOR\"" || failc)
    biobrainJson=$(echo "$biobrainJson" | jq ".roleId = \"$ROLE_ID\"" || failc)
    biobrainJson=$(echo "$biobrainJson" | jq ".serialNumber = \"$SERIAL_NUMBER\"" || failc)
    biobrainJson=$(echo "$biobrainJson" | jq ".statusId = \"hardware-status-harrisburg-inventory\"" || failc)

    log "Sending $biobrainJson"

    response=$(apiCurl -o ${TEMPORARY_WORKING_DIRECTORY}/response -w '%{response_code}' -X ${updateMethod} --data-binary "$biobrainJson" ${updateUrl})

    if [[ "$response" != "200" ]]; then
        fail "Updating Cloud Info Failed: $(cat ${TEMPORARY_WORKING_DIRECTORY}/response)"
    fi
}

function uploadLogs() {
    step "Uploading logs"

    gzip ${LOG_FILE} || failc
    /opt/bht/biobrain/bin/uploadlogs.sh || failc
}

function waitForNetwork() {
    network=""
    sleepTime=0

    while [[ "$network" == "" ]]; do
        sleep ${sleepTime}
        log "Waiting for network..."
        curl -s -L "https://biobrain.biohitechcloud.com/init/biobrain-install" > /dev/null

        if [[ "$?" == "0" ]]; then
            network="1"
        fi
        sleepTime=5
    done
}

function validateInitProperties() {
#    validateContains DEVICE_TYPE "${DEVICE_TYPE:-Unknown}" "${VALID_DEVICE_TYPES}" || return 1
#    validateContains ROLE_ID "${ROLE_ID:-Unknown}" "${VALID_ROLE_IDS}" || return 1
#    validateContains HARDWARE_MODEL "${HARDWARE_MODEL:-Unknown}" "${VALID_HARDWARE_MODELS}" || return 1
#    validateContains HARDWARE_VENDOR "${HARDWARE_VENDOR:-Unknown}" "${VALID_HARDWARE_VENDORS}" || return 1
#    validateContains ORDER_VENDOR "${ORDER_VENDOR:-Unknown}" "${VALID_ORDER_VENDORS}" || return 1

    validateNotEmpty DEVICE_TYPE "${DEVICE_TYPE}" || fail "DEVICE_TYPE not found"
    validateNotEmpty BIOBRAIN_SOFTWARE_VERSION "${BIOBRAIN_SOFTWARE_VERSION}" || fail "BIOBRAIN_SOFTWARE_VERSION not found"
    validateNotEmpty HARDWARE_MODEL "${HARDWARE_MODEL}" || fail "HARDWARE_MODEL not found"
    validateNotEmpty HARDWARE_VENDOR "${HARDWARE_VENDOR}" || fail "HARDWARE_VENDOR not found"
    validateNotEmpty INSTALL_ID "${INSTALL_ID}" || fail "INSTALL_ID not found"
    validateNotEmpty MACHINE_KEY "${MACHINE_KEY}" || fail "MACHINE_KEY not found"
    validateNotEmpty ORDER_NUMBER "${ORDER_NUMBER}" || fail "ORDER_NUMBER not found"
    validateNotEmpty ORDER_VENDOR "${ORDER_VENDOR}" || fail "ORDER_VENDOR not found"
    validateNotEmpty ROLE_ID "${ROLE_ID}" || fail "ROLE_ID not found"
}


# endregion

# region Utility Functions

function curlc() {
    curl -s -L -H "User-Agent: ${BASH_SOURCE[1]} $MACHINE_KEY" "$@"
}

function curlJson() {
    curlc -H 'Content-Type: application/json;charset=UTF-8' -H 'Accept: application/json' "$@"
}

function deployCurl() {
    # last argument
    url=$(urlQualify "${@: -1}" ${DEPLOYMENT_BASE_URL})

    # remove last argument
    set -- "${@: 1: $#-1}"

    curlJson -H "$API_KEY_HEADER" "$@" ${url}
}

function deployApiCurl() {
    # last argument
    url=$(urlQualify "${@: -1}" ${DEPLOYMENT_API_BASE_URL})

    # remove last argument
    set -- "${@: 1: $#-1}"

    curlJson -H "$API_KEY_HEADER" "$@" ${url}
}


function apiCurl() {
    # last argument
    url=$(urlQualify "${@: -1}" ${API_BASE_URL})

    # remove last argument
    set -- "${@: 1: $#-1}"

    curlJson -H "$API_KEY_HEADER" "$@" ${url}
}

function fail() {
    error="$1"
    func=${2:-${FUNCNAME[1]}}
    line=${3:-${BASH_LINENO[0]}}

    if [[ "$error" != "" ]]; then
        log__ "ERROR" ${func} ${line} ${error}
    fi

    failMessage="Script Failed"

    if [[ "$SCRIPT_FAIL_MESSAGE" != "" ]]; then
        failMessage="$SCRIPT_FAIL_MESSAGE"
    fi

    logf ${failMessage}

    if [[ "$SCRIPT_FAIL_FUNCTION" != "" ]]; then
        ${SCRIPT_FAIL_FUNCTION} "${error}"
    fi

    kill -s TERM $$
    return 1
}

function failc() {
    line=${BASH_LINENO[0]}
    func=$(sed "${line}q;d" ${BASH_SOURCE[0]})
    fail "Command Failed - $func" ${FUNCNAME[1]} ${BASH_LINENO[0]}
}


function httpStatusCheck() {
    validStatusCodes=$1
    statusCode=$2

    if [[ ! -t 0 ]]; then
        statusCode=$(cat -)
    fi

    if [[ "$statusCode" == "" ]]; then
        statusCode="$validStatusCodes"
        validStatusCodes=""
    fi

    if [[ "$validStatusCodes" == "" ]]; then
        validStatusCodes="200"
    fi

    if [[ "$validStatusCodes" != *"$statusCode"* ]]; then
        return 1
    fi

    return 0
}

function jsonEncode () {
    printf '%s' "$1" | python -c 'import json,sys; print(json.dumps(sys.stdin.read()))'
}

function log__() {
    level=$1
    func=$2
    line=$3
    message="${@:4}"

    if [[ "$level" == "" ]]; then
        level="INFO"
    fi

    if [[ "$__BASENAME" == "" ]]; then
        __BASENAME=$(basename ${BASH_SOURCE[0]})
    fi

    echo "[log] $(date -u +"%Y-%m-%dT%H:%M:%SZ") $level $__BASENAME:$func():$line : $message"
}

function log() {
    log__ "INFO" ${FUNCNAME[1]} ${BASH_LINENO[0]} "$@"
}

function logd() {
    log__ "DEBUG" ${FUNCNAME[1]} ${BASH_LINENO[0]} "$@"
}

function loge() {
    log__ "ERROR" ${FUNCNAME[1]} ${BASH_LINENO[0]} "$@"
}

function logf() {
    log__ "FATAL" ${FUNCNAME[1]} ${BASH_LINENO[0]} "$@"
}

function logi() {
    log__ "INFO" ${FUNCNAME[1]} ${BASH_LINENO[0]} "$@"
}

function logw() {
    log__ "WARN" ${FUNCNAME[1]} ${BASH_LINENO[0]} "$@"
}

function pass() {
    passMessage="Script Finished"

    if [[ "$SCRIPT_PASS_MESSAGE" != "" ]]; then
        passMessage="$SCRIPT_PASS_MESSAGE"
    fi

    logi ${passMessage}

    if [[ "$SCRIPT_PASS_FUNCTION" != "" ]]; then
        ${SCRIPT_PASS_FUNCTION} ${passMessage}
    fi

    return 0
}

function readProperties() {
    if [[ -f "$1" ]]; then
        while IFS='=' read -r key value; do
            key=$(echo ${key} | tr '.' '_')

            if [[ "$key" != "" ]]; then
                eval ${key}=\$value
            fi
        done < "$1"
    else
        return 1
    fi
}

function rootCheck() {
    step "Performing Root Check"

    if [[ "$EUID" -ne 0 ]]; then
        fail "This script must be run as root"
    fi
}

function scriptInit() {
    trap 'exit 1' TERM
}

function sshCloud() {
    # TODO: don't use password
    sshpass -p 'edwinhoch!!!' ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -tt bht@www.biohitechcloud.com -tt ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "$@"
}

function step() {
    message="$1"
    notify="${2:-false}"
    log__ "INFO" ${FUNCNAME[1]} ${BASH_LINENO[0]} "$message"

    json="{\"status\":$(jsonEncode "$message"),\"notify\":$notify}"

    if [[ "$INSTALL_ID" != "" ]]; then
        deployApiCurl -w '%{response_code}' --data-binary "${json}" /1.0/installs/${INSTALL_ID}/status > /dev/null
    fi
}

function trim() {
    value="$1"

    if [[ ! -t 0 ]]; then
        value=$(cat -)
    fi

    echo -n "$(echo "$value" | sed -E 's/^[[:space:]]*//g' |  sed -E 's/[[:space:]]*$//g')"
}

function validateNotEmpty() {
    name=$1
    value=$2

    if [[ "$value" == "" ]]; then
        log__ "ERROR" ${FUNCNAME[1]} ${BASH_LINENO[0]} "$name is empty"
        return 1
    fi
}

function validateContains() {
    name=$1
    value=$2
    haystack=$3

    if [[ "$value" == "" ]] || [[ "$haystack" != *"$value"* ]]; then
        log__ "ERROR" ${FUNCNAME[1]} ${BASH_LINENO[0]} "Invalid $name, '$value' is not in [$haystack]"
        return 1
    fi
}

function urlQualify() {
    url=$1
    base=$2

    if [[ "${url:0:1}" == "/" ]]; then
        url="$base$url"
    fi

    echo -n ${url}
}
# endregion

main "$@"
