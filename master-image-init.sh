#!/bin/bash

###############################################################################
# Creates a master BioBrain image from a base Ubuntu image.
###############################################################################

# region Global Variables

OS_VERSION=$(lsb_release -a 2>/dev/null | grep ^Release: | awk '{print $2}')
OS_VERSION_MAJOR=$(echo "$OS_VERSION" | cut -d. -f1)
OS_VERSION_MINOR=$(echo "$OS_VERSION" | cut -d. -f2)
UBUNTU_18_OR_NEWER=$(expr "$OS_VERSION_MAJOR" '>=' '18')
UBUNTU_12_OR_OLDER=$(expr "$OS_VERSION_MAJOR" '<=' '12')

MASTER_IMAGE_VERSION=""
PROPERTIES_FILE=/var/tmp/biobrain/init.properties


SCRIPT_FAIL_MESSAGE='Master Image Initialization Failed'
SCRIPT_PASS_MESSAGE='Master Image Initialized'

# endregion

# region Script Functions

function main() {
    if [[ "$(tty)" != "/dev/tty1" ]]; then
        exit
    fi

    scriptInit
    log "Initializing Master Image"
    rootCheck
    loadInitProperties
    createDirectories
    installPackages
    copyEFIBoot
    configureNetwork
    disableInternetChatter
    configureAutoLogin
    writeImageProperties
    cleanup
    sync
    pass
}


function cleanup() {
    log "Moving Master Image Init properties to /opt/bht/install"
    mv "$PROPERTIES_FILE" /opt/bht/install/master-image-init.properties

    log "Moving Master Image Log to /opt/bht/logs"
    mv /var/tmp/biobrain/master-image-init.log /opt/bht/logs/master-image-init.log.1

    log "Copying this script to /opt/bht/install"
    cp "${BASH_SOURCE[0]}" /opt/bht/install

    log "Removing Auto-Run Script"
    sed -i -E 's/^.*\/master-image-prompt.sh$//g' ~/.bashrc

    rm -rf /var/tmp/biobrain

    chown -R "bht:bht" "/opt/bht" || failc
}

function configureAutoLogin() {
    log "Configuring Automatic Login"

    if [[ "$UBUNTU_18_OR_NEWER" == "1" ]]; then
        cp /lib/systemd/system/getty@.service /lib/systemd/system/getty@.service.orig || failc
        sed -i -E 's/^(ExecStart=.*)$/#\1\nExecStart=-\/sbin\/mingetty --autologin root --noclear %I/g' /lib/systemd/system/getty@.service || failc
    else
        cp /etc/init/tty1.conf /etc/init/tty1.conf.orig
        sed -i -E 's/^exec[[:space:]]+\/sbin\/getty.*$/exec \/sbin\/mingetty --autologin root --noclear tty1/g' /etc/init/tty1.conf
    fi
}

function configureNetwork() {
    log "Configuring Network."

    if [[ "$UBUNTU_18_OR_NEWER" == "1" ]]; then
        cp /etc/cloud/cloud.cfg /etc/cloud/cloud.cfg.orig || failc
        sed -i -E 's/^(preserve_hostname:\s*).*$/\1true/g' /etc/cloud/cloud.cfg || failc

        cp /lib/systemd/system/systemd-networkd-wait-online.service /lib/systemd/system/systemd-networkd-wait-online.service.orig || failc
        sed -i -E 's/^(ExecStart=.*)$/\1 --timeout=10/g' /lib/systemd/system/systemd-networkd-wait-online.service || failc
    fi
}

function copyEFIBoot() {
    log "Copying EFI boot file"
    mkdir -p /boot/efi/EFI/BOOT || failc

    if [[ -f /boot/efi/EFI/ubuntu/grubx64.efi ]]; then
        cp /boot/efi/EFI/ubuntu/grubx64.efi /boot/efi/EFI/BOOT/bootx64.efi || failc
    fi
}

function createDirectories() {
    log "Creating temporary working directory"
    mkdir -p /tmp/biobrain || failc

    log "Creating bht directories"
    mkdir -p /opt/bht/etc || failc
    mkdir -p /opt/bht/install || failc
    mkdir -p /opt/bht/logs || failc
}


function disableInternetChatter() {
    log "Disabling Automatic Updates"

    if [[ -f /etc/apt/apt.conf.d/10periodic ]]; then
        sed -i 's/APT::Periodic::Update-Package-Lists "1";/APT::Periodic::Update-Package-Lists "0";/g' /etc/apt/apt.conf.d/10periodic || failc
    fi

    if [[ -f /etc/apt/apt.conf.d/20auto-upgrades ]]; then
        sed -i 's/APT::Periodic::Update-Package-Lists "1";/APT::Periodic::Update-Package-Lists "0";/g' /etc/apt/apt.conf.d/20auto-upgrades || failc
        sed -i 's/APT::Periodic::Unattended-Upgrade "1";/APT::Periodic::Unattended-Upgrade "0";/g' /etc/apt/apt.conf.d/20auto-upgrades || failc
    fi

    if [[ "$UBUNTU_18_OR_NEWER" == "1" ]]; then
        systemctl stop snapd.service || failc
        systemctl disable snapd.service || failc
        systemctl mask snapd.service || failc
        systemctl daemon-reload || failc
        mkdir -p /var/tmp/motd || failc
        mv /etc/update-motd.d/* /var/tmp/motd || failc

        log "Disabling Message of the Day"
        sed -i -E 's/^ENABLED=1/ENABLED=0/g' /etc/default/motd-news || failc
    fi

    log "Removing Whoopsie"
    update-rc.d -f whoopsie remove
}


function installPackage() {
    packageName=$1
    log "Installing $packageName"
    apt-get install -y ${packageName} || failc
}

function installPackages() {
    killAptDaily
    updateApt

    installSalt

    installPackage curl
    installPackage dnsutils

    apt-get purge -y default-jre-headless > /dev/null 2>&1
    apt-get purge -y openjdk* > /dev/null 2>&1

    if [[ "$UBUNTU_18_OR_NEWER" == "1" ]]; then
        installPackage openjdk-11-jdk-headless
    elif [[ "$UBUNTU_12_OR_OLDER" == "1" ]]; then
        installPackage openjdk-7-jre-headless
    else
        installPackage openjdk-7-jdk-headless
    fi

    installPackage hddtemp
    installPackage jq
    installPackage mingetty
    installPackage lm-sensors
    installPackage openssh-server
    installPackage python-psutil
    installPackage sshpass
    installPackage telnet
    installPackage uuid

    if [[ "$UBUNTU_18_OR_NEWER" == "1" ]]; then
        installPackage yq
    fi

    installPackage zip

    upgradePackages
}

function installSalt() {
    if [[ "$UBUNTU_12_OR_OLDER" == "1" ]]; then
        installPackage salt-minion
    else
        curl -s -L https://bootstrap.saltstack.com -o /tmp/biobrain/install_salt.sh || failc
        sudo sh /tmp/biobrain/install_salt.sh -P || failc
    fi
}
function installSaltRepo() {
    log "Installing Salt Repo"

    if [[ "$UBUNTU_12_OR_OLDER" == "1" ]]; then
        echo "deb http://repo.saltstack.com/apt/ubuntu/12.04/amd64/latest precise main" |  tee /etc/apt/sources.list.d/saltstack.list || failc
        wget -q -O- https://repo.saltstack.com/apt/ubuntu/12.04/amd64/latest/SALTSTACK-GPG-KEY.pub | apt-key add - || failc

    fi
}

function loadInitProperties() {
    log "Loading Properties $PROPERTIES_FILE"
    readProperties ${PROPERTIES_FILE} || failc
    validateInitProperties || fail "1 or more properties is invalid"
}

function killAptDaily() {
    if [[ "$UBUNTU_18_OR_NEWER" == "1" ]]; then
        log "Killing Apt Daily Service"

        systemctl stop apt-daily.timer || failc
        systemctl disable apt-daily.timer || failc
        systemctl mask apt-daily.timer || failc

        systemctl stop apt-daily.service || failc
        systemctl disable apt-daily.service || failc
        systemctl mask apt-daily.service || failc

        ps -ef | grep apt | grep -v grep | awk '{print $2}' | xargs kill > /dev/null 2>&1
        rm /var/lib/dpkg/lock*
        rm /var/cache/apt/archives/lock*

        log "Sleeping for 30 seconds to make sure Apt Daily Service is killed"
        sleep 30
        ps -ef | grep apt | grep -v grep | awk '{print $2}' | xargs kill > /dev/null 2>&1
        rm /var/lib/apt/lists/lock*
        rm /var/lib/dpkg/lock*
        rm /var/cache/apt/archives/lock*
        dpkg --configure -a
    fi
}

function updateApt() {
    log "Updating Apt"

    if [[ "$UBUNTU_18_OR_NEWER" == "1" ]]; then
        sudo add-apt-repository -y ppa:rmescandon/yq || failc
    fi

    if [[ "$UBUNTU_12_OR_OLDER" == "1" ]]; then
        add-apt-repository -y ppa:openjdk-r/ppa || failc
    fi

    installSaltRepo

    apt-get update -y --fix-missing || failc
    apt-get update -y || failc
    apt-get autoremove -y || failc
}

function upgradePackages() {
    log "Upgrading Packages"
    apt-get upgrade -y || failc
}

function validateInitProperties() {
    validateNotEmpty MASTER_IMAGE_VERSION  $MASTER_IMAGE_VERSION || return 1
}

function waitForNetwork() {
    network=""
    sleepTime=0

    while [[ "$network" == "" ]]; do
        sleep ${sleepTime}
        log "Waiting for network..."
        curl -s -L "https://biobrain.biohitechcloud.com/init/biobrain-install" > /dev/null

        if [[ "$?" == "0" ]]; then
            network="1"
        fi
        sleepTime=5
    done
}

function writeImageProperties() {
    echo "image.version=$MASTER_IMAGE_VERSION" > /opt/bht/etc/static.properties || failc
}

# endregion

# region Utility Functions

function fail() {
    error="$1"
    func=${2:-${FUNCNAME[1]}}
    line=${3:-${BASH_LINENO[0]}}

    if [[ "$error" != "" ]]; then
        log__ "ERROR" ${func} ${line} ${error}
    fi

    failMessage="Script Failed"

    if [[ "$SCRIPT_FAIL_MESSAGE" != "" ]]; then
        failMessage="$SCRIPT_FAIL_MESSAGE"
    fi

    logf ${failMessage}

    if [[ "$SCRIPT_FAIL_FUNCTION" != "" ]]; then
        ${SCRIPT_FAIL_FUNCTION} "${error}"
    fi

    kill -s TERM $$
    return 1
}

function failc() {
    line=${BASH_LINENO[0]}
    func=$(sed "${line}q;d" ${BASH_SOURCE[0]})
    fail "Command Failed - $func" ${FUNCNAME[1]} ${BASH_LINENO[0]}
}

function httpStatusCheck() {
    validStatusCodes=$1
    statusCode=$2

    if [[ ! -t 0 ]]; then
        statusCode=$(cat -)
    fi

    if [[ "$statusCode" == "" ]]; then
        statusCode="$validStatusCodes"
        validStatusCodes=""
    fi

    if [[ "$validStatusCodes" == "" ]]; then
        validStatusCodes="200"
    fi

    if [[ "$validStatusCodes" != *"$statusCode"* ]]; then
        return 1
    fi

    echo -n ""

    return 0
}


function log__() {
    level=$1
    func=$2
    line=$3
    message="${@:4}"

    if [[ "$level" == "" ]]; then
        level="INFO"
    fi

    if [[ "$__BASENAME" == "" ]]; then
        __BASENAME=$(basename ${BASH_SOURCE[0]})
    fi

    echo "[log] $(date -u +"%Y-%m-%dT%H:%M:%SZ") $level $__BASENAME:$func():$line : $message"
}

function log() {
    log__ "INFO" ${FUNCNAME[1]} ${BASH_LINENO[0]} "$@"
}

function logd() {
    log__ "DEBUG" ${FUNCNAME[1]} ${BASH_LINENO[0]} "$@"
}

function loge() {
    log__ "ERROR" ${FUNCNAME[1]} ${BASH_LINENO[0]} "$@"
}

function logf() {
    log__ "FATAL" ${FUNCNAME[1]} ${BASH_LINENO[0]} "$@"
}

function logi() {
    log__ "INFO" ${FUNCNAME[1]} ${BASH_LINENO[0]} "$@"
}

function logw() {
    log__ "WARN" ${FUNCNAME[1]} ${BASH_LINENO[0]} "$@"
}


function pass() {
    passMessage="Script Finished"

    if [[ "$SCRIPT_PASS_MESSAGE" != "" ]]; then
        passMessage="$SCRIPT_PASS_MESSAGE"
    fi

    logi ${passMessage}

    if [[ "$SCRIPT_PASS_FUNCTION" != "" ]]; then
        ${SCRIPT_PASS_FUNCTION} ${passMessage}
    fi

    return 0
}

function readProperties() {
    if [[ -f "$1" ]]; then
        while IFS='=' read -r key value; do
            key=$(echo ${key} | tr '.' '_')

            if [[ "$key" != "" ]]; then
                eval ${key}=\$value
            fi
        done < "$1"
    else
        return 1
    fi
}

function rootCheck() {
    if [[ "$EUID" -ne 0 ]]; then
        fail "This script must be run as root"
    fi
}

function scriptInit() {
    trap 'exit 1' TERM
}

function validateNotEmpty() {
    name=$1
    value=$2

    if [[ "$value" == "" ]]; then
        log__ "ERROR" ${FUNCNAME[1]} ${BASH_LINENO[0]} "$name is empty"
        return 1
    fi
}

# endregion

main "$@"
