#!/usr/bin/env bash

###############################################################################
# Verifies the BioBrain initialization Process
###############################################################################

# region Global Variables

OS_VERSION=$(lsb_release -a 2>/dev/null | grep ^Release: | awk '{print $2}')
OS_VERSION_MAJOR=$(echo "$OS_VERSION" | cut -d. -f1)
OS_VERSION_MINOR=$(echo "$OS_VERSION" | cut -d. -f2)
UBUNTU_18_OR_NEWER=$(expr "$OS_VERSION_MAJOR" '>=' '18')

NETWORK_TYPE=""

# endregion

# region Script Functions

function main() {
    waitForNetwork
    scriptInit
    rootCheck
    getNetworkType
    interfaceScriptTest
    wiredInterfaceTest
    plcInterfaceTest
    wirelessInterfaceTest
    biobrainPidTest
    date '+%Y-%m-%dT%H:%M:%S' > /opt/bht/etc/BIOBRAIN_VERIFIED || failc
    log "All Tests Passed."
}

function getNetworkType {
    wifiModule=$(cat /opt/bht/etc/local.properties  | grep -ioE "^modules=.*" | grep -io "wifi")
    wifiFile="/opt/bht/etc/networks/wireless_interfaces"

    if [[ -n "$wifiModule" ]]; then
        ([[ -f  ${wifiFile} ]] && NETWORK_TYPE="wireless") || fail "wifi module configured, but $wifiFile does not exist."
    elif [[ -f ${wifiFile} ]]; then
        fail "$wifiFile exists, but wifi modules is not configured."
    else
        NETWORK_TYPE="wired"
    fi

}

function plcInterfaceTest {
    plcInterfaceConfigured=""

    if [[ "$UBUNTU_18_OR_NEWER" == "1" ]]; then
        plcInterfaceConfigured=$(cat /etc/netplan/99-bht-plc.yaml | grep -o 25.25.25.200)
    else
        plcInterfaceConfigured=$(cat /etc/network/interfaces | grep -o 25.25.25.200)
    fi

    plcInterfaceRequired=$(cat /opt/bht/etc/local.properties  | grep -ioE "^modules=.*" | grep -ioE "allen-bradley|xgb")

    if [[ -z "$plcInterfaceRequired" ]]; then
        log "PLC Interface is not required.  Skipping PLC Interface tests."
        return
    fi

    if [[ -z "$plcInterfaceConfigured" ]]; then
        fail "PLC Interface is required, but not configured."
        return 1
    fi

    plcIsPluggedIn=""
    sleepTime=0

    while [[ -z "$plcIsPluggedIn" ]]; do
        sleep ${sleepTime}

        if [[ -n "$(ifconfig | grep -io "25.25.25.200")" && "$(ifconfig | grep -io "25.25.25.200")" == "25.25.25.200" ]]; then
            plcIsPluggedIn=1
        else
            log "Waiting for PLC cable to be plugged in"
        fi

        sleepTime=5
    done

    log "PLC Interface is UP"
}

function wirelessInterfaceTest {

    if [[ "$NETWORK_TYPE" != "wireless" ]]; then
        return
    fi

    log "Testing wireless interface is configured"

    if [[ -z "$(cat /opt/bht/etc/networks/wireless_interfaces | grep -io "bhtwlan0")" ]]; then
        fail "/opt/bht/etc/networks/wireless_interfaces does not exist or is not appropriately configured"
        return 1
    fi

    log "Testing wireless interface is up"

    if [[ -z "$(ifconfig | grep -io bhtwlan0)" ]]; then
        fail  "Wireless interface is not up.  You may need to reboot to fix this issue."
    fi
}

function wiredInterfaceTest {
    if [[ "$NETWORK_TYPE" != "wired" ]]; then
        return
    fi

    if [[ -z "$( ifconfig | grep -ioP "inet.*?(\d{1,3}\.){3}\d{1,3}" | grep -ivP "127.0.0.1|25.25.25.200" | sed 's/inet addr://g')" ]]; then
        fail "Wired interface is not up."
    fi

    log "Testing for internet. Pinging google"
    pingTest=$(ping -c 10 google.com | grep -io "Destination host unreachable" | tail -1)

    if [[ -z "$pingTest" ]]; then
        log "Ping of google successful"
    else
        fail "Unable to ping google"
        return 1
    fi
}


function biobrainPidTest {
    pid=""
    log "Looking for BioBrain PID"
    startTime=$(date +%s)
    log "Start Time: $startTime"

    while [[ -z ${pid} ]]; do
        if [[ "$(echo "$(date +%s) - $startTime" | bc)" -gt "60" ]]; then
            fail "Could not find BioBrain PID | BioBrain not running..."
        fi
        log "Time Elapsed $(echo "$(date +%s) - $startTime" | bc) seconds\\r"
        pid=$(ps auxwww | grep java | grep BioBrain | grep -v grep | awk '{print $2}')
    done
    log "Found BioBrain pid $pid"
}

function interfaceScriptTest {
    internet=$(bash /opt/bht/biobrain/bin/interfaces.sh -p)

    if [[ -z "$internet" ]]; then
        fail "Unable to find Internet Interface"
    fi

    log "Internet Interface: $internet"

    if [[ ! -z $(cat /opt/bht/etc/local.properties  | grep -i modules | egrep -io "allen-bradley|xgb") ]]; then
        plc=$(bash /opt/bht/biobrain/bin/interfaces.sh -s)

        if [[ -z ${plc} ]]; then
            fail "Unable to find PLC Interface"
        fi

        if [[ "$internet" == "$plc" ]]; then
            fail "Internet and PLC interfaces are the same: $internet"
        fi

        log "PLC Interface: $plc"
    fi
}

# endregion

# region Utility Functions

function curlc() {
    curl -s -L -H "User-Agent: ${BASH_SOURCE[1]}" "$@"
}

function curlJson() {
    curlc -H 'Content-Type: application/json;charset=UTF-8' -H 'Accept: application/json' "$@"
}

function deployApiCurl() {
    # last argument
    url=$(urlQualify "${@: -1}" ${DEPLOYMENT_API_BASE_URL})

    # remove last argument
    set -- "${@: 1: $#-1}"

    curlJson -H "$API_KEY_HEADER" "$@" ${url}
}


function apiCurl() {
    # last argument
    url=$(urlQualify "${@: -1}" ${API_BASE_URL})

    # remove last argument
    set -- "${@: 1: $#-1}"

    curlJson -H "$API_KEY_HEADER" "$@" ${url}
}

function detectNumberOfEthernetPorts() {
    NUM_ETHERNET_PORTS=$(lspci | grep -i eth | wc -l || failc)
}


function fail() {
    error="$1"
    func=${2:-${FUNCNAME[1]}}
    line=${3:-${BASH_LINENO[0]}}

    if [[ "$error" != "" ]]; then
        log__ "ERROR" ${func} ${line} ${error}
    fi

    failMessage="Script Failed"

    if [[ "$SCRIPT_FAIL_MESSAGE" != "" ]]; then
        failMessage="$SCRIPT_FAIL_MESSAGE"
    fi

    logf ${failMessage}

    if [[ "$SCRIPT_FAIL_FUNCTION" != "" ]]; then
        ${SCRIPT_FAIL_FUNCTION} "${error}"
    fi

    kill -s TERM $$
    return 1
}

function failc() {
    line=${BASH_LINENO[0]}
    func=$(sed "${line}q;d" ${BASH_SOURCE[0]})
    fail "Command Failed - $func" ${FUNCNAME[1]} ${BASH_LINENO[0]}
}


function httpStatusCheck() {
    validStatusCodes=$1
    statusCode=$2

    if [[ ! -t 0 ]]; then
        statusCode=$(cat -)
    fi

    if [[ "$statusCode" == "" ]]; then
        statusCode="$validStatusCodes"
        validStatusCodes=""
    fi

    if [[ "$validStatusCodes" == "" ]]; then
        validStatusCodes="200"
    fi

    if [[ "$validStatusCodes" != *"$statusCode"* ]]; then
        return 1
    fi

    return 0
}

function jsonEncode () {
    printf '%s' "$1" | python -c 'import json,sys; print(json.dumps(sys.stdin.read()))'
}

function log__() {
    level=$1
    func=$2
    line=$3
    message="${@:4}"

    if [[ "$level" == "" ]]; then
        level="INFO"
    fi

    if [[ "$__BASENAME" == "" ]]; then
        __BASENAME=$(basename ${BASH_SOURCE[0]})
    fi

    echo "[log] $(date -u +"%Y-%m-%dT%H:%M:%SZ") $level $__BASENAME:$func():$line : $message"
}

function log() {
    log__ "INFO" ${FUNCNAME[1]} ${BASH_LINENO[0]} "$@"
}

function logd() {
    log__ "DEBUG" ${FUNCNAME[1]} ${BASH_LINENO[0]} "$@"
}

function loge() {
    log__ "ERROR" ${FUNCNAME[1]} ${BASH_LINENO[0]} "$@"
}

function logf() {
    log__ "FATAL" ${FUNCNAME[1]} ${BASH_LINENO[0]} "$@"
}

function logi() {
    log__ "INFO" ${FUNCNAME[1]} ${BASH_LINENO[0]} "$@"
}

function logw() {
    log__ "WARN" ${FUNCNAME[1]} ${BASH_LINENO[0]} "$@"
}

function pass() {
    passMessage="Script Finished"

    if [[ "$SCRIPT_PASS_MESSAGE" != "" ]]; then
        passMessage="$SCRIPT_PASS_MESSAGE"
    fi

    logi ${passMessage}

    if [[ "$SCRIPT_PASS_FUNCTION" != "" ]]; then
        ${SCRIPT_PASS_FUNCTION} ${passMessage}
    fi

    return 0
}

function readProperties() {
    if [[ -f "$1" ]]; then
        while IFS='=' read -r key value; do
            key=$(echo ${key} | tr '.' '_')

            if [[ "$key" != "" ]]; then
                eval ${key}=\$value
            fi
        done < "$1"
    else
        return 1
    fi
}

function rootCheck() {
    step "Performing Root Check"

    if [[ "$EUID" -ne 0 ]]; then
        fail "This script must be run as root"
    fi
}

function scriptInit() {
    trap 'exit 1' TERM
}

function sshCloud() {
    # TODO: don't use password
    sshpass -p 'edwinhoch!!!' ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -tt bht@www.biohitechcloud.com -tt ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "$@"
}

function step() {
    message="$1"
    notify="${2:-false}"
    log__ "INFO" ${FUNCNAME[1]} ${BASH_LINENO[0]} "$message"

    json="{\"status\":$(jsonEncode "$message"),\"notify\":$notify}"

    if [[ "$INSTALL_ID" != "" ]]; then
        deployApiCurl -w '%{response_code}' --data-binary "${json}" /1.0/installs/${INSTALL_ID}/status > /dev/null
    fi
}

function validateNotEmpty() {
    name=$1
    value=$2

    if [[ "$value" == "" ]]; then
        log__ "ERROR" ${FUNCNAME[1]} ${BASH_LINENO[0]} "$name is empty"
        return 1
    fi
}

function validateContains() {
    name=$1
    value=$2
    haystack=$3

    if [[ "$value" == "" ]] || [[ "$haystack" != *"$value"* ]]; then
        log__ "ERROR" ${FUNCNAME[1]} ${BASH_LINENO[0]} "Invalid $name, '$value' is not in [$haystack]"
        return 1
    fi
}

function urlQualify() {
    url=$1
    base=$2

    if [[ "${url:0:1}" == "/" ]]; then
        url="$base$url"
    fi

    echo -n ${url}
}

function waitForNetwork() {
    network=""
    sleepTime=0

    while [[ "$network" == "" ]]; do
        sleep ${sleepTime}
        log "Waiting for network..."
        curl -s -L "https://biobrain.biohitechcloud.com/init/biobrain-install" > /dev/null

        if [[ "$?" == "0" ]]; then
            network="1"
        fi
        sleepTime=5
    done
}
#endregion

main "$@"




