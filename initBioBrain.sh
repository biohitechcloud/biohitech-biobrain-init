#!/bin/bash

###############################################################################
# WARNING: THIS IS A LEGACY FILE.  PLEASE MAKE FURTHER UPDATES to biobrain-init.sh
###############################################################################

##################################
#        Global Variables        #
##################################
old=""
wireless=""
debugMode="$1"

readonly thisScript=$(basename $0 | sed 's/\.sh//g')
readonly default="\033[39m"
readonly black="\033[30m"
readonly red="\033[31m"
readonly green="\033[32m"
readonly yellow="\033[33m"
readonly blue="\033[34m"
readonly argsLength=$#


##################################
#           Functions            #
##################################
function log () {
    case "$1" in
        err)
            echo -e    "$red[$( date )] [$(basename $0)] [ERROR  ]$default $2" | tee -a /tmp/biobrain-init.log
            ;;
        info)
              echo -e "$blue[$( date )] [$(basename $0)] [INFO   ]$default $2" | tee -a /tmp/biobrain-init.log
            ;;
        scss)
             echo -e "$green[$( date )] [$(basename $0)] [SUCCESS]$default $2" | tee -a /tmp/biobrain-init.log
            ;;
        warn)
            echo -e "$yellow[$( date )] [$(basename $0)] [WARNING]$default $2" | tee -a /tmp/biobrain-init.log
            ;;
        dbg)
            if [[ "$debugMode" == "debug" || "$debugMode" == "-d" ]]; then
                echo -e "$yellow[$( date )] [$(basename $0)] [DEBUG  ]$default $2" | tee -a /tmp/biobrain-init.log
            fi
            ;;
    esac
}

compareVersions () {
    if [[ $1 == $2 ]]
    then
        return 0
    fi
    local IFS=.
    local i ver1=($1) ver2=($2)
    # fill empty fields in ver1 with zeros
    for ((i=${#ver1[@]}; i<${#ver2[@]}; i++))
    do
        ver1[i]=0
    done
    for ((i=0; i<${#ver1[@]}; i++))
    do
        if [[ -z ${ver2[i]} ]]
        then
            # fill empty fields in ver2 with zeros
            ver2[i]=0
        fi
        if ((10#${ver1[i]} > 10#${ver2[i]}))
        then
            return 1
        fi
        if ((10#${ver1[i]} < 10#${ver2[i]}))
        then
            return 2
        fi
    done
    return 0
}



function chowner () {
    chown -R "$1:$1" "$2"
    if [[ "$?" != "0" ]]; then
        log err "Unable to chown $2"
    fi
}

sendFile(){
	TYPE=$1
	ACTUAL_LOG_FILE=$2
	LOG_FILE_PATH=$3

	curl -s -i -k --header 'x-biohitech-auth: CJeDC5JfT32sN35XmKxf' -F "name=$TYPE.log" -F "logFile=$ACTUAL_LOG_FILE" --connect-timeout 10 --output /tmp/upload_logs_output --write-out %{http_code} "https://digester-logs.biohitechcloud.com/zippedLogs/$MACHINE_ID/$TYPE" > /tmp/uploadlogs_http_status_code
	CURL_STATUS=$?
	if [ "$CURL_STATUS" -gt 0 ]; then
		rm -f /tmp/uploadlogs_http_status_code 2> /dev/null
		echo "[$(date)] : uploadlogs.sh : Error reaching Log Upload URL : curl status : $CURL_STATUS" >> /opt/bht/logs/script.log
		return 1
	fi
	#
	# Make sure that we have an HTTP Status Code of 200
	#
	HTTP_STATUS_CODE=$(cat /tmp/uploadlogs_http_status_code)
	log "status code = $HTTP_STATUS_CODE"
	rm -f /tmp/uploadlogs_http_status_code 2> /dev/null
	if [ "$HTTP_STATUS_CODE" -ne '200' ]; then
		echo "[$(date)] : uploadlogs.sh : Invalid HTTP Status Code : $HTTP_STATUS_CODE" >> /opt/bht/logs/script.log
		return 1
	else
		return 0
	fi
}


upload() {
	OLD_LOG_FILES=$(ls "$BIOBRAIN_LOG_DIR/$1.log."* 2> /dev/null)

	for FILE in $OLD_LOG_FILES;do
        log "Processing log file : $FILE"
		actualsize=$(du -k "$FILE" | cut -f1)
		if [ "$actualsize" -lt 1000 ]; then
			params=($1 @$FILE $FILE)
			if sendFile ${params[@]}; then
			    log "Success sending $FILE"
				rm -f "$FILE"
			else
			    log "Failed sending $FILE"
				continue
			fi
		else
		    log "File: $FILE is too big, trimming"
			number=$(tr '.' $' ' <<< "$FILE" | awk '{print $3}')
            tmpDir=/tmp
			tmpLogFileName=$1.log.$number

            gunzip -c $FILE | head -5000 >> $tmpDir/$tmpLogFileName
			gunzip -c $FILE | tail -5000 >> $tmpDir/$tmpLogFileName
			gzip $tmpDir/$tmpLogFileName

			newLogFileGzip=$1.log.$number.gz

            cp $tmpDir/$newLogFileGzip $BIOBRAIN_LOG_DIR

			params=($1 @$BIOBRAIN_LOG_DIR/$newLogFileGzip $BIOBRAIN_LOG_DIR/$newLogFileGzip)
			if sendFile ${params[@]}; then
			    log "Success sending $BIOBRAIN_LOG_DIR/$newLogFileGzip"
				rm -f "$FILE"
			else
			    log "Failed sending $BIOBRAIN_LOG_DIR/$newLogFileGzip"
			fi

			rm -f $tmpDir/$newLogFileGzip
		fi
	done
}


function wifiConfig () {
    # Backing up interface config
    log info "Backing up interface config"
    mkdir -p /opt/bht/etc/networks
    cp /etc/network/interfaces /opt/bht/etc/networks/interfaces.orig
    cp /etc/network/interfaces /opt/bht/etc/networks/wireless_interfaces
    cp /etc/network/interfaces /opt/bht/etc/networks/accesspoint_interfaces

    # Adds necessary config for each interface mode
    log info  "Adds necessary config for each interface mode"
    echo 'auto bhtwlan0'>> /opt/bht/etc/networks/interfaces.orig
    echo 'iface bhtwlan0 inet dhcp'>> /opt/bht/etc/networks/interfaces.orig

    echo 'allow-hotplug bhtwlan0'>> /opt/bht/etc/networks/wireless_interfaces
    echo 'auto bhtwlan0'>> /opt/bht/etc/networks/wireless_interfaces
    echo 'iface bhtwlan0 inet dhcp'>> /opt/bht/etc/networks/wireless_interfaces
    echo '   wpa-conf /etc/wpa_supplicant.conf'>> /opt/bht/etc/networks/wireless_interfaces
    echo '   pre-up iptables-restore < /opt/bht/biobrain/etc/iptables.bht.rules' >> /opt/bht/etc/networks/wireless_interfaces

    echo 'allow-hotplug bhtwlan0' >> /opt/bht/etc/networks/accesspoint_interfaces
    echo 'iface bhtwlan0 inet static' >> /opt/bht/etc/networks/accesspoint_interfaces
    echo '   address 172.24.1.1' >> /opt/bht/etc/networks/accesspoint_interfaces
    echo '   netmask 255.255.255.0' >> /opt/bht/etc/networks/accesspoint_interfaces
    echo '   network 172.24.1.0' >> /opt/bht/etc/networks/accesspoint_interfaces
    echo '   broadcast 172.24.1.255' >> /opt/bht/etc/networks/accesspoint_interfaces
    echo '   pre-up iptables-restore < /opt/bht/biobrain/etc/iptables.bht.wifi.rules' >> /opt/bht/etc/networks/accesspoint_interfaces

    rm /etc/network/interfaces
    cp /opt/bht/etc/networks/wireless_interfaces /etc/network/interfaces

    sed -i '/modules=/ s/$/, wifi/' /opt/bht/etc/local.properties

    wpa_passphrase foo foofoofoo > /opt/bht/etc/networks/empty_wpa_supplicant.conf
    ln -sfn /opt/bht/etc/networks/empty_wpa_supplicant.conf /etc/wpa_supplicant.conf

    chowner "root" "/opt/bht/etc/networks/empty_wpa_supplicant.conf"


    # Setting up SSID
    log info "Setting up SSID"
    echo "ssid=BioHiTech-$(ifconfig -a | grep -i -E 'wlan0' | tr -d ':' | awk '{print $NF}' | cut -c7-)" >> /etc/hostapd/hostapd.conf

}

function getSecondaryInterface(){
	interface=$(ifconfig -a | grep -iE "8c:ae:4c|00:50:B6" | awk '{print $1}')
	if [[ -z "$interface" ]]; then
		log err "Unable to find Secondary Interface"
		exit 1
	else
		log info "Found interface: $interface"
		return 0
	fi
}

function getMacAddress() {
	#  this fuction should be revisited if ever the script is unable to retreive a mac address
	macFilter='b827eb|F44D30|ECA86B|C89CDC|C03FD5|B8AEED|7427EA|4487FC|1078D2|002511|002197|001E90|001BB9|001921|0016EC|00142A|00115B|001035|000D87|000AE6|000795|94C691'
	macAddress=$(ifconfig -a | sed 's/://g' | egrep -i "$macFilter" | awk '{print $NF}' | awk '{print toupper($0)}')
	if [[ -z "$macAddress" ]] || [[ "$macAddress" == "" ]]; then
		log err "Unable to find a mac address"
		exit 1
	else
	    echo "$macAddress"
		return 0
	fi
}

function setMachineId () {
    machineId=""
    confirmMachineId=""
	while :
	do
		while [ -z "$machineId" ]; do
			read -p "Please Enter the Machine Id: " machineId
		done

		while [[ -z "$confirmMachineId" ]]; do
			read -p "Confirm Machine Id: " confirmMachineId
		done

		if [[ "$machineId" = "$confirmMachineId" ]];
		then
			echo "Machine Id Confirmed: $confirmMachineId"
            MACHINE_ID=$confirmMachineId
    		break;
		fi

		unset machineId
		unset confirmMachineId
		log warn "Ids do not match."

	done
}

function setCloudInfo () {
	cloudInfo=()
	#  Order Vendor
	while [[ -z "${cloudInfo[0]}" ]]; do
		read -p "Please specify the Order Vendor (1 for GDC, 2 for Amazon): " cloudInfo[0]

		if [[ "${cloudInfo[0]}" = "1" ]]; then
			log info "Order Vendor:" "GDC"
			cloudInfo[0]='GDC'
		elif [[ "${cloudInfo[0]}" = "2" ]]; then
			log info "Order Vendor:" "Amazon"
			cloudInfo[0]='Amazon'
    	else
			cloudInfo[0]=""
			log warn "Invalid Order Vendor"
    	fi
    done

	#  Hardware Model
	while [ -z "${cloudInfo[1]}" ]; do
		read -p "Please specify the Hardware Model (1 for BOXNUC5CPYH, 2 for DCCP847DYE): " cloudInfo[1]

		if [[ "${cloudInfo[1]}" = "1" ]]; then
			log info "Hardware Model:\tBOXNUC5CPYH"
			cloudInfo[1]='BOXNUC5CPYH'
		elif [[ "${cloudInfo[1]}" = "2" ]]; then
			log info "Hardware Model:\tDCCP847DYE"
			cloudInfo[1]='DCCP847DYE'
		else
			cloudInfo[1]=""
			log warn "Invalid Device Type."
		fi
	done

	#  Role
	while [ -z "${cloudInfo[2]}" ]; do
		read -p "Please specify the Role (1 for Machine, 2 for Development/Test, 3 for Scale, 4 for Signage): " cloudInfo[2]
		if [[ "${cloudInfo[2]}" = "1" ]]; then
			log info "Role:\tMachine"
			cloudInfo[2]='machine'
		elif [[ "${cloudInfo[2]}" = "2" ]]; then
			log info "Role:\tDevelopment Test"
			cloudInfo[2]='development'
		elif [[ "${cloudInfo[2]}" = "3" ]]; then
			log info "Role:\tScale"
			cloudInfo[2]='scale'
		elif [[ "${cloudInfo[2]}" = "4" ]];
		then
			log info "Role:\tSignage"
			cloudInfo[2]='signage'
		else
			cloudInfo[2]=""
			log warn "Invalid Device Type."
		fi
	done

	# Serial number
    cloudInfo[3]=$(dmidecode -s baseboard-serial-number | grep -v '#')
    echo -e "'\033[1;33mSerial number:\e[0m\t' ${cloudInfo[3]}"

	# Order Number
	while [ -z "${cloudInfo[4]}" ]; do
		echo -e "Please specify the '\033[1;33morder\e[0m' number: "
		read cloudInfo[4]
	done
}

function setDeviceType () {
	deviceTypeName=""
	deviceType=""
	while :
	do
		while [ -z "$deviceType" ]; do
			read -p "Please specify Device Type (1 for Master-K, 2 for XGB, 3 for Allen Bradley, 4 for Baler): " deviceType
		done

		if [[ "$deviceType" = "1" ]];
		then
			echo "Using Device type Master-K"
			deviceTypeName='masterk'
    		break;
		fi

		if [[ "$deviceType" = "2" ]];
		then
			echo "Using Device type XGB"
			deviceTypeName='xgb'
			getSecondaryInterface
    		break;
		fi

		if [[ "$deviceType" = "3" ]];
		then
			echo "Using Device type Allen Bradley"
			deviceTypeName='ab'
            getSecondaryInterface
    		break;
		fi

		if [[ "$deviceType" = "4" ]];
		then
			echo "Using Device type Baler"
			deviceTypeName='scale'
    		break;
		fi

		if [[ "$deviceType" = "5" ]];
		then
			echo "Using Device type Kiosk"
			deviceTypeName='kiosk'
    		break;
		fi

		unset deviceType
		echo "Invalid Device Type."

	done
}

function updateCloudInfo () {
    export PYTHONIOENCODING=utf8

    macAddress=$(getMacAddress)
    bbSearch=$(curl -X GET -H "x-biohitech-auth-token: dd7a0d38-1a8d-45b2-8f73-9d8aaa683447" https://api.biohitechcloud.com/internal/v1/biobrains/$machineId 2>/dev/null)
    serialNumber=$(echo $bbSearch | python -c "import sys, json; print json.load(sys.stdin)['serialNumber']")
    biobrainId=$(echo $bbSearch | python -c "import sys, json; print json.load(sys.stdin)['id']")
    hardwareVendor=$(echo $bbSearch | python -c "import sys, json; print json.load(sys.stdin)['hardwareVendor']")
    hardwareModel=$(echo $bbSearch | python -c "import sys, json; print json.load(sys.stdin)['hardwareModel']")
    orderNumber=$(echo $bbSearch | python -c "import sys, json; print json.load(sys.stdin)['orderNumber']")
    orderVendor=$(echo $bbSearch | python -c "import sys, json; print json.load(sys.stdin)['orderVendor']")
    description=$(echo $bbSearch | python -c "import sys, json; print json.load(sys.stdin)['description']")
    statusId="hardware-status-harrisburg-inventory"
    roleId="machine"

    log dbg "macAddress:\t$macAddress"
    log dbg "bbSearch:\t$bbSearch"
    log dbg "machineId:\t$machineId"
    log dbg "serialNumber:\t$serialNumber"
    log dbg "biobrainId:\t$biobrainId"
    log dbg "hardwareVendor:\t$hardwareVendor"
    log dbg "hardwareModel:\t$hardwareModel"
    log dbg "orderNumber:\t$orderNumber"
    log dbg "orderVendor:\t$orderVendor"
    log dbg "statusId:\t$statusId"
    log dbg "roleId:\t$roleId"
    log dbg "description:\t$description"

    curl "https://api.biohitechcloud.com/internal/v1/biobrains/$biobrainId" -X PUT -H 'Origin: https://biobrain.biohitechcloud.com' -H 'Accept-Encoding: gzip, deflate, sdch, br' -H 'Accept-Language: en-US,en;q=0.8' -H "User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.75 Safari/537.36 bht/$biobrainId/$machineKey" -H 'x-biohitech-auth-token: dd7a0d38-1a8d-45b2-8f73-9d8aaa683447' -H 'Content-Type: application/json;charset=UTF-8' -H 'Accept: application/json, text/plain, */*' -H 'Referer: https://biobrain.biohitechcloud.com/biobrains' -H 'DNT: 1' -H 'Connection: keep-alive' -H 'x-biohitech-source: admin-web' --data-binary "{\"id\": \"$biobrainId\",\"machineKey\": \"$machineId\",\"serialNumber\": \"$serialNumber\",\"macAddress\": \"$macAddress\",\"hardwareVendor\": \"$hardwareVendor\",\"hardwareModel\": \"$hardwareModel\",\"orderNumber\": \"$orderNumber\",\"orderVendor\": \"$orderVendor\",\"roleId\": \"$roleId\",\"deleted\": false,\"buildTypeId\": \"$deviceTypeName\",\"statusId\": \"hardware-status-harrisburg-inventory\",\"description\": \"$description\",\"buildDate\": \"$(date +%Y-%m-%d)\"}" --compressed

    log dbg "https://api.biohitechcloud.com/internal/v1/biobrains/$biobrainId -X PUT -H 'Origin: https://biobrain.biohitechcloud.com' -H 'Accept-Encoding: gzip, deflate, sdch, br' -H 'Accept-Language: en-US,en;q=0.8' -H User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.75 Safari/537.36 bht/$biobrainId/$machineKey -H 'x-biohitech-auth-token: dd7a0d38-1a8d-45b2-8f73-9d8aaa683447' -H 'Content-Type: application/json;charset=UTF-8' -H 'Accept: application/json, text/plain, */*' -H 'Referer: https://biobrain.biohitechcloud.com/biobrains' -H 'DNT: 1' -H 'Connection: keep-alive' -H 'x-biohitech-source: admin-web' --data-binary {\"id\": \"$biobrainId\",\"machineKey\": \"$machineId\",\"serialNumber\": \"$serialNumber\",\"macAddress\": \"$macAddress\",\"hardwareVendor\": \"$hardwareVendor\",\"hardwareModel\": \"$hardwareModel\",\"orderNumber\": \"$orderNumber\",\"orderVendor\": \"$orderVendor\",\"roleId\": \"$roleId\",\"deleted\": false,\"buildTypeId\": \"$deviceTypeName\",\"statusId\": \"hardware-status-harrisburg-inventory\",\"description\": \"$description\",\"buildDate\": \"$(date +%Y-%m-%d)\"}"

}

function bhtDirectoryCreator {
    # Setup installation directories
    log info "Setting up bht directories"
    mkdir /opt/bht
    mkdir /opt/bht/etc
    mkdir /opt/bht/var
    mkdir /opt/bht/var/software
    mkdir /opt/bht/var/software/staged
    mkdir /opt/bht/var/software/installed
    mkdir /opt/bht/biobrain
    mkdir /opt/bht/logs
    mkdir /opt/bht/events
}

function networkConfiguration {
    # Set DNS Servers
    log info "Setting alternate DNS Servers to 8.8.8.8, 8.8.4.4"
    echo "append domain-name-servers 8.8.8.8, 8.8.4.4;" >> /etc/dhcp/dhclient.conf

    # Set IP Tables configuration (for Salt re-routing)
    echo "  pre-up iptables-restore < /opt/bht/biobrain/etc/iptables.bht.rules" >> /etc/network/interfaces

    # Set up $interface for XGB PLC
    if [[ "$deviceType" = "2" ]];
    then
        getSecondaryInterface
        log info "Configuring static IP for $interface (XGB PLCs only)"
        sed -i 's/auto $interface/ /g' /etc/network/interfaces
        sed -i 's/iface $interface inet dhcp/ /g' /etc/network/interfaces
        echo "Setting up interface: $interface"
        echo "auto $interface" >> /etc/network/interfaces
        echo "iface $interface inet static" >> /etc/network/interfaces
        echo "  address 25.25.25.200" >> /etc/network/interfaces
        echo "  netmask 255.255.255.0" >> /etc/network/interfaces
        log info "Done setting up interface: $interface"
    fi

    # Set up $interface for Allen Bradley PLC
    if [[ "$deviceType" = "3" ]];
    then
        getSecondaryInterface
        log info "Configuring static IP for $interface (Allen Bradley PLCs only)"
        sed -i 's/auto $interface/ /g' /etc/network/interfaces
        sed -i 's/iface $interface inet dhcp/ /g' /etc/network/interfaces
        echo "auto $interface" >> /etc/network/interfaces
        echo "iface $interface inet static" >> /etc/network/interfaces
        echo "  address 25.25.25.200" >> /etc/network/interfaces
        echo "  netmask 255.255.255.0" >> /etc/network/interfaces
        log info "Done setting up interface: $interface"
    fi
}

function setSalt {
    # Edit Salt minion
    log info "Configuring Salt minion"
    mkdir -p /opt/bht/etc/salt/minion
    echo "id: $machineId" > /opt/bht/etc/salt/minion/bht-local.conf
    echo "master: digester-salt-master.biohitechcloud.com" >> /opt/bht/etc/salt/minion/bht-remote.conf
    echo "master_port: 443" >> /opt/bht/etc/salt/minion/bht-remote.conf
    echo "grains:" >> /opt/bht/etc/salt/minion/bht-remote.conf
    echo "  roles:" >> /opt/bht/etc/salt/minion/bht-remote.conf
    echo "    - biobrain" >> /opt/bht/etc/salt/minion/bht-remote.conf
    echo "    - nuc" >> /opt/bht/etc/salt/minion/bht-remote.conf
    echo "auth_timeout: 60" >> /opt/bht/etc/salt/minion/bht-remote.conf
    echo "random_reauth_delay: 60" >> /opt/bht/etc/salt/minion/bht-remote.conf
    echo "recon_default: 1000" >> /opt/bht/etc/salt/minion/bht-remote.conf
    echo "recon_max: 60000" >> /opt/bht/etc/salt/minion/bht-remote.conf
    echo "recon_randomize: True" >> /opt/bht/etc/salt/minion/bht-remote.conf
    ln -s /opt/bht/etc/salt/minion/bht-local.conf /etc/salt/minion.d/01-bht-local.conf
    ln -s /opt/bht/etc/salt/minion/bht-remote.conf /etc/salt/minion.d/02-bht-remote.conf
    chmod 660 /opt/bht/etc/salt/minion/bht-local.conf
    chmod 660 /opt/bht/etc/salt/minion/bht-remote.conf
    chowner "bht" "/opt/bht/etc/salt/minion"
}

function setSudo {
    # Setup sudo permissions for bht
    log info "Setting up sudo permissions for bht and bhtadmin"
    echo "bht  ALL= NOPASSWD: /opt/bht/biobrain/bin/run.sh" >> /etc/sudoers.d/bht
    echo "bht  ALL= NOPASSWD: /opt/bht/biobrain/bin/hid-linux" >> /etc/sudoers.d/bht
    echo "bht  ALL= NOPASSWD: /bin/date" >> /etc/sudoers.d/bht
    echo "bht  ALL= NOPASSWD: /bin/chmod" >> /etc/sudoers.d/bht
    echo "bht  ALL= NOPASSWD: /tmp/script.sh" >> /etc/sudoers.d/bht
    echo "bht  ALL= NOPASSWD: /opt/bht/biobrain/bin/net-config-static.sh" >> /etc/sudoers.d/bht
    echo "bht  ALL= NOPASSWD: /opt/bht/biobrain/bin/net-config-dhcp.sh" >> /etc/sudoers.d/bht
    echo "bht  ALL= NOPASSWD: /opt/bht/biobrain/bin/check-temp.sh" >> /etc/sudoers.d/bht
    echo "Defaults visiblepw" >> /etc/sudoers.d/bht
    echo "bhtadmin ALL=(ALL)  NOPASSWD: ALL" >> /etc/sudoers.d/bhtadmin
    chmod 0440 /etc/sudoers.d/*
}

function congigureHostname {
    # Change hostname to machineId
    log info "Changing hostname to machineId"
    echo "$machineId" > /etc/hostname
    service hostname start
    echo "127.0.0.1  $machineId" >> /etc/hosts
}

function configureOldProps {
    log info "Configuring old properties"
    # Creating local properties file
    log info "Creating local.properties"
    touch /opt/bht/etc/local.properties

    echo "machineId=$machineId" >> /opt/bht/etc/local.properties


    if [[ "$deviceType" = "1" ]];
    then
        echo "modules=masterk, remote-control" >> /opt/bht/etc/local.properties
    fi

    if [[ "$deviceType" = "2" ]];
    then
        echo "modules=xgb, remote-control" >> /opt/bht/etc/local.properties
    fi

    if [[ "$deviceType" = "3" ]];
    then
        echo "modules=allen-bradley, remote-control" >> /opt/bht/etc/local.properties
    fi

    if [[ "$deviceType" = "4" ]];
    then
        echo "modules=baler, remote-control" >> /opt/bht/etc/local.properties
    fi

    if [[ "$deviceType" = "5" ]];
    then
        echo "modules=signage, remote-control" >> /opt/bht/etc/local.properties
    fi

    if [[ "$deviceType" == "3" ]]; then
        while [[ -z "$wireless" ]]; do
            read -p $'\nWill the device need wifi? [Y/n] ' wireless

            if [[ "$wireless" == "Y" || "$wireless" == "y" || "$wireless" == "yes" ]]; then
                echo "Setting up wireless configuration."
                wifiConfig
                break;
            elif [[ "$wireless" == "N" || "$wireless" == "n" || "$wireless" == "no" ]]; then
                break;
            fi

            unset wirelss
        done
    fi
}

function configureNewProps {
    log info "Configuring new properties"
    # Creating static properties file
    log info "Creating static.properties"
    touch /opt/bht/etc/static.properties
    # Creating local properties file
    log info "Creating local.properties"
    touch /opt/bht/etc/local.properties

    echo "machineId=$machineId" >> /opt/bht/etc/static.properties
    if [[ "$deviceType" = "1" ]];
    then
        echo "modules=masterk, remote-control" >> /opt/bht/etc/local.properties
    fi

    if [[ "$deviceType" = "2" ]];
    then
        echo "modules=xgb, remote-control" >> /opt/bht/etc/local.properties
    fi

    if [[ "$deviceType" = "3" ]];
    then
        echo "modules=allen-bradley, remote-control" >> /opt/bht/etc/local.properties
    fi

    if [[ "$deviceType" = "4" ]];
    then
        echo "modules=baler, remote-control" >> /opt/bht/etc/local.properties
    fi

    if [[ "$deviceType" = "5" ]];
    then
        echo "modules=signage, remote-control" >> /opt/bht/etc/local.properties
    fi

    if [[ "$deviceType" == "3" ]]; then
        while [[ -z "$wireless" ]]; do
            read -p $'\nWill the device need wifi? [Y/n] ' wireless

            if [[ "$wireless" == "Y" || "$wireless" == "y" || "$wireless" == "yes" ]]; then
                echo "Setting up wireless configuration."
                wifiConfig
                break;
            elif [[ "$wireless" == "N" || "$wireless" == "n" || "$wireless" == "no" ]]; then
                break;
            fi

            unset wirelss
        done
    fi

    log info "Getting api token."
    token=$(curl -s -X POST https://deploy.biohitechcloud.com/init/$machineId/accessToken)
    tokenKey=$(echo $token | python -c "import sys, json; print json.load(sys.stdin)['tokenKey']")

    log info "Setting api token in local properties"
    echo "api.key=${tokenKey}" >> /opt/bht/etc/local.properties
}

function setPropFile {

    if [[ $version == "latest" ]]; then
        configureNewProps
    else
        majorVersion="${version:0:1}"
        if [[ "$majorVersion" == "8" || "$majorVersion" == "7" ]]; then
            compareVersions $version '8.0.0'
            result=$?
            case $result in
                0) configureNewProps;;
                1) configureNewProps;;
                2) configureOldProps;;
            esac
        elif [[ "$majorVersion" == "6" ]]; then
            compareVersions $version '6.5.8'
            result=$?
            case $result in
                0) configureNewProps;;
                1) configureNewProps;;
                2) configureOldProps;;
            esac
        fi
    fi
}

function installBiobrain {

    version=""

    while [[ $HTTP_STATUS_CODE -ne '200' ]]; do
        echo -n -e "Installing BioBrain: Press enter for the latest release or specify one: "
        read version
        if [[ -z "$version" ]]; then
            version="latest"
        fi
        HTTP_STATUS_CODE=$(curl -s -o /tmp/manifest --write-out %{http_code} --connect-timeout 5 https://digester-admin.biohitechcloud.com/releases/biobrain/$version/manifest)
        RC=$?
        log info "HTTP_STATUS_CODE:\t$HTTP_STATUS_CODE"

        # Make sure curl returns successfully
        if [[ $RC -gt 0 ]]; then
            log err "Error accessing https://digester-admin.biohitechcloud.com/releases/biobrain/$version/manifest"
            log info "Return code:\t$RC"
            exit 1;
        fi

        # Checking Status Code
        if [[ $HTTP_STATUS_CODE -ne '200' ]]; then
            log err "Invalid HTTP Status Code = $HTTP_STATUS_CODE, URL = https://digester-admin.biohitechcloud.com/releases/biobrain/$version/manifest"
        fi

        if [[ $HTTP_STATUS_CODE == '404' ]]; then
            log err "$version does not exist! Please Try Again."
        fi
    done

    # Install Biobrain
    log info "Installing BioBrain"
    HTTP_STATUS_CODE=$(curl -s -o /tmp/biohitech-biobrain.tar.gz --write-out %{http_code} --connect-timeout 5 https://digester-admin.biohitechcloud.com/releases/biobrain/$version/biohitech-biobrain.tar.gz)
    # Make sure curl returns successfully
    if [[ $? -gt 0 ]]; then
        log err "Error accessing https://digester-admin.biohitechcloud.com/releases/biobrain/$version/biohitech-biobrain.tar.gz"
        exit 1;
    fi
    # Checking Status Code
    if [[ $HTTP_STATUS_CODE -ne '200' ]]; then
        log err "Invalid HTTP Status Code = $HTTP_STATUS_CODE, URL = https://digester-admin.biohitechcloud.com/releases/biobrain/$version/biohitech-biobrain.tar.gz"
        exit 1;
    fi

    # Check MD5SUMS
    DOWNLOAD_MD5=$(md5sum "/tmp/biohitech-biobrain.tar.gz" | cut -d' ' -f1)
    MANIFEST_MD5=$(cat /tmp/manifest | grep "biohitech-biobrain.tar.gz" | cut -d' ' -f1)
    rm -f /tmp/manifest

    if [[ "$DOWNLOAD_MD5" != "$MANIFEST_MD5" ]];
    then
        echo "manifest and biohitech-biobrain.tar.gz do not match."
        exit 1;
    fi

    log info "Unzipping biohitech-biobrain.tar.gz"

    cd /opt/bht/biobrain/ || log err "Unable to change directories"
    tar zxvf /tmp/biohitech-biobrain.tar.gz
    rm -f /tmp/biohitech-biobrain.tar.gz
}

function linkInitScripts {
    # Link the init scripts
    log info "Linking init scripts"
    ln -sf /opt/bht/biobrain/etc/init/biobrain.conf /etc/init/biobrain.conf
    ln -sf /opt/bht/biobrain/etc/init/serial-tty.conf /etc/init/serial-tty.conf
    chmod 744 /etc/init/biobrain.conf
    chmod 744 /etc/init/serial-tty.conf
}

function installCron {
    # Install root cron
    log info "Installing root cron"
    echo "0 */4 * * * /opt/bht/biobrain/bin/sleepupto.sh 600 && /bin/su - -c 'service salt-minion restart' > /dev/null 2>&1" >> /var/spool/cron/crontabs/root
    chmod 600 /var/spool/cron/crontabs/root

    # Install bht cron jobs
    log info "Installing bht cron jobs"
    cp /opt/bht/biobrain/etc/crontab.example /var/spool/cron/crontabs/bht
    chowner "bht" "/var/spool/cron/crontabs/bht"
    chmod 600  /var/spool/cron/crontabs/bht
}

function configureGrub {
    # Configure Grub
    log info "Configuring grub..."
    echo " " >> /etc/default/grub
    echo "# Make sure unit boots automatically after a failure" >> /etc/default/grub
    echo "GRUB_RECORDFAIL_TIMEOUT=5" >> /etc/default/grub
    update-grub
}

function restartNetwork {
    # Restart Networking Services
    log info "Restarting networking services"
    ifdown -a
    ifup -a
}

function configSalt {
    # Restart Salt Minion
    log info "Restarting Salt Minion"
    service salt-minion restart
    # Accepts Salt Key on Salt Master
    ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -tt bht@www.biohitechcloud.com -tt ssh digester-salt-master-prod.bht "sudo ./machineKey.sh $machineId"
    # Highstates BioBrain
    salt-call state.highstate
}

function rootCheck {
    # Check to see if the script was run as Root
    if [[ "$EUID" -ne 0 ]]; then
        log err  "Sorry, you need to run this as root"
        exit 1
    fi
}

function hasBeenInited {
    while [[ -z "$old" ]]; do
        echo -n -e "Was this device previouslt init'ed? [Y/n]\t"
        read old
        if [[ "$old" == "y" ]] || [[ "$old" == "Y" ]]; then
            old=true
            break;
        elif [[ "$old" == "n" ]] || [[ "$old" == "N" ]]; then
            old=false
            break;
        else
            unset old
        fi
    done
}

# End Functions

log dbg "Debugging:\tENABLED"

#
# Set up BIOBRAIN_LOG_DIR
#
if [ -z "$BIOBRAIN_LOG_DIR" ]; then
	export BIOBRAIN_LOG_DIR=/opt/bht/logs
fi
log "Using BIOBRAIN_LOG_DIR = $BIOBRAIN_LOG_DIR"

#
# Set up BIOBRAIN_ETC_DIR
#
if [ -z "$BIOBRAIN_ETC_DIR" ]; then
	export BIOBRAIN_ETC_DIR=/opt/bht/etc
fi
log "Using BIOBRAIN_ETC_DIR = $BIOBRAIN_ETC_DIR"

# Upload the logs!
upload biobrain-init

if [[ "$argsLength" -gt "1" || ( ! -z "$debugMode" && "$debugMode" != "debug" && "$debugMode" != "-d" ) ]]; then
    log err "Invalid arguments"
    echo -e "Ussage:\n\tNORMAL:\n\t\t$(basename $0)\n\tDEBUG:\n\t\t$(basename $0) -d\n\tOR\n\t\t$(basename $0) debug"
    exit 1
fi

rootCheck
hasBeenInited
setDeviceType
setMachineId
getMacAddress

if [[ "$old" == "false" ]]; then
	setCloudInfo
fi

# Removing Duplicate Salt key
if [[ "$old" == "true" ]]; then
	ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -tt bht@www.biohitechcloud.com -tt ssh digester-salt-master-prod.bht sudo salt-key -d $machineId -y
fi

congigureHostname
networkConfiguration

# Turn Automatic updates off
sed -i 's/APT::Periodic::Update-Package-Lists "1";/APT::Periodic::Update-Package-Lists "0";/g' /etc/apt/apt.conf.d/10periodic


# Set the timezone to UTC
log info "Setting localtime to UTC"
ln -sf /usr/share/zoneinfo/UTC /etc/localtime

# Set FSCKFIX=yes
log info "Setting FSCKFIX=yes"
sed -i 's/FSCKFIX=no/FSCKFIX=yes/g' /etc/default/rcS

bhtDirectoryCreator
setSalt
setSudo
installBiobrain
setPropFile

# Set DHCP Timeout parameters
log info "Setting DHCP Client timeout parameters"
echo "timeout 60;" >> /etc/dhcp/dhclient.conf
echo "retry 15;" >> /etc/dhcp/dhclient.conf

# Give permission to tty serial ports
log info "Giving permission to tty serial ports"
chmod 777 /dev/ttyUSB0
chmod 777 /dev/ttyUSB1

linkInitScripts
installCron
configureGrub

# Remove Whoopsie
log info "Removing Whoopsie"
update-rc.d -f whoopsie remove


# Make sure that everything under /opt/bht is owned by bht!
touch /opt/bht/logs/script.log
chowner "bht" "/opt/bht"

restartNetwork
configSalt

# Adding the udev rules
log info "Adding udev rules"
cp /opt/bht/biobrain/etc/udev/rules.d/99-*.rules /etc/udev/rules.d/

# Performs a time sync and writes changes to hardware clock
source /opt/bht/biobrain/bin/timesync.sh
hwclock -w

# Adding to Cloud
if [[ "$old" == "true" ]]; then
    updateCloudInfo
elif [[ "$old" == "false" ]]; then
	log info "Adding to BioHiTech Cloud"
	curl -X POST 'https://api.biohitechcloud.com/internal/v1/biobrains' -H 'x-biohitech-auth-token: dd7a0d38-1a8d-45b2-8f73-9d8aaa683447' -H 'Content-Type: application/json;charset=UTF-8' -H 'Accept: application/json, text/plain, */*' -H 'Referer: https://admin.biohitechcloud.com/biobrains' --data-binary "{\"statusId\":\"hardware-status-harrisburg-inventory\",\"buildDate\":\"$(date +%Y-%m-%d)\",\"machineKey\":\"$machineId\",\"serialNumber\":\"${cloudInfo[3]}\",\"macAddress\":\"$macAddress\",\"hardwareVendor\":\"Intel\",\"hardwareModel\":\"${cloudInfo[1]}\",\"orderNumber\":\"${cloudInfo[4]}\",\"orderVendor\":\"${cloudInfo[0]}\",\"buildTypeId\":\"$deviceTypeName\",\"roleId\":\"${cloudInfo[2]}\",\"description\":\"\"}"
fi


# Clearing old updates
log info "Clearing old updates"
curl -X POST https://deploy.biohitechcloud.com/init/$machineId/cleanup

log info "copying biobrain-init.log to /opt/bht/logs/biobrain-init.log"
cp /tmp/biobrain-init.log /opt/bht/logs/
chowner "bht" "/opt/bht/logs/biobrain-init.log"
gzip /opt/bht/logs/biobrain-init.log
rm -rf /opt/bht/logs/biobrain-init.log
upload biobrain-init


# Retrives and runs post-init-test.sh
curl -s -L http://bit.ly/post-init-test > post-init-test.sh
chmod +x post-init-test.sh
./post-init-test.sh
gzip /opt/bht/logs/post-init-test.log
rm -rf /opt/bht/logs/post-init-test.log
upload post-init-test


# Syncing File System
sync

read -p "Press enter to reboot:  "
reboot
